﻿using System;
using NUnit.Framework;
using Tondo.WebApp.Core;
using Tondo.WebApp.Core.Http;
using Tondo.WebApp.Sevices.VendorYandex;

namespace Tondo.Tests
{
    [TestFixture]
    public class VendorYandexServiceTest
    {
        private VendorYandexService _service;

        [SetUp]
        public void Initialize()
        {
            _service = new VendorYandexService(new HttpClientService(), "https://vendor.eda.yandex/api/v1", new AuthInfo(){Name = "tondomoscow@gmail.com", Password = "p4k2PSjDFG" });
        }

        [Test]
        public void LogiTest()
        {
            _service.Login();
        }
    }
}
