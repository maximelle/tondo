﻿using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Models
{
    public class IikoProduct : EntityBase
    {
        public string Name { get; set; }
        public bool IsDeleted { get; set; }

        public string Type { get; set; }
        public string GroupId { get; set; }
        public virtual IikoGroup Group { get; set; }

        public virtual YandexProduct YandexProduct { get; set; }
    }
}
