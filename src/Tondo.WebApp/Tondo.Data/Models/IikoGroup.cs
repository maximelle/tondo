﻿using System.Collections.Generic;
using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Models
{
    public class IikoGroup  :EntityBase
    {
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string ParentGroupId { get; set; }

        public virtual List<IikoProduct> Products { get; set; }
    }
}
