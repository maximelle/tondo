﻿using System.Collections.Generic;
using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Models
{
    public class YandexGroup : EntityBase
    {
        public int PlaceId { get; set; }
        public string Name { get; set; }
        public bool Available { get; set; }

        public virtual List<YandexProduct> Products { get; set; }
    }
}
