﻿namespace Tondo.Data.Models.Abstract
{
    public abstract class EntityBase : IEntity
    {
        public string Id { get; set; }
    }
}
