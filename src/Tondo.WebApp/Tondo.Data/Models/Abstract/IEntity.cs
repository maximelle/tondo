﻿namespace Tondo.Data.Models.Abstract
{
    public interface IEntity
    {
        public string Id { get; set; }
    }
}
