﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tondo.Data.Models
{
    public class YandexBinding
    {
        public int Id { get; set; }
        public string IikoId { get; set; }
        public string YandexId { get; set; }
    }
}
