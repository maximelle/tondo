﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Models
{
    public class DeliveryClubGroup : EntityBase
    {
        public int DeliveryClubId { get; set; }
        public string ExternalId { get; set; }
        public string Name { get; set; }

        public string VendorId { get; set; }
        //public virtual DeliveryClubVendor Vendor { get; set; }

        public virtual List<DeliveryClubProduct> Products { get; set; }
    }
}
