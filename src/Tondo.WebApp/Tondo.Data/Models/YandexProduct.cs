﻿using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Models
{
    public class YandexProduct : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string IikoProductId { get; set; }
        public virtual IikoProduct IikoProduct { get; set; }

        public string GroupId { get; set; }
        public bool Available { get; set; }

        public int PlaceId { get; set; }
        public string Type { get; set; }

        public int MenuItemId { get; set; }

        public virtual YandexPlace Place { get; set; }
        public virtual YandexGroup Group { get; set; }
    }
}
