﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Models
{
    public class DeliveryClubProduct : EntityBase
    {
        public int DeliveryClubId { get; set; }
        public string ExternalId { get; set; }
        public string Name { get; set; }

        public string Type { get; set; }

        public string IikoProductId { get; set; }
        public virtual IikoProduct IikoProduct { get; set; }

        public string VendorId { get; set; }
        public virtual DeliveryClubVendor Vendor { get; set; }

        public string GroupId { get; set; }
        public virtual DeliveryClubGroup Group { get; set; }
    }
}
