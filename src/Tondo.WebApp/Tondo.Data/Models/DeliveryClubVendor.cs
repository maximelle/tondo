﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Models
{
    public class DeliveryClubVendor : EntityBase
    {
        public int VendorId { get; set; }
        public string Name { get; set; }
        public int ChainId { get; set; }
        public string ChainName { get; set; }

        public virtual List<DeliveryClubProduct> Products { get; set; }
    }
}
