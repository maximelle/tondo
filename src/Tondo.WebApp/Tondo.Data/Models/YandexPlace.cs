﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tondo.Data.Models
{
    public class YandexPlace
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Available { get; set; }

        public virtual List<YandexProduct> Products { get; set; }
    }
}
