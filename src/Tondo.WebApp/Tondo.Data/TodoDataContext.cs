﻿using System;
using Microsoft.EntityFrameworkCore;
using Tondo.Data.Configuration;
using Tondo.Data.Models;

namespace Tondo.Data
{
    public class TodoDataContext : DbContext
    {
        public DbSet<YandexGroup> YandexGroups { get; set; }
        public DbSet<YandexProduct> YandexProducts { get; set; }
        public DbSet<YandexPlace> YandexPlaces { get; set; }

        public DbSet<IikoGroup> IikoGroups { get; set; }
        public DbSet<IikoProduct> IikoProducts { get; set; }

        public DbSet<YandexBinding> YandexBindings { get; set; }

        public DbSet<YandexOrder> YandexOrders { get; set; }
        
        public DbSet<DeliveryClubVendor> DeliveryClubVendors { get; set; }
        public DbSet<DeliveryClubGroup> DeliveryClubGroups { get; set; }
        public DbSet<DeliveryClubProduct> DeliveryClubProducts { get; set; }

        public TodoDataContext(DbContextOptions options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new YandexGroupConfiguration());
            modelBuilder.ApplyConfiguration(new YandexProductConfiguration());
            modelBuilder.ApplyConfiguration(new YandexPlaceConfiguration());

            modelBuilder.ApplyConfiguration(new IikoGroupConfiguration());
            modelBuilder.ApplyConfiguration(new IikoProductConfiguration());

            modelBuilder.ApplyConfiguration(new YandexBindingConfiguration());
            modelBuilder.ApplyConfiguration(new YandexOrderConfiguration());

            modelBuilder.ApplyConfiguration(new DeliveryVendorConfiguration());
            modelBuilder.ApplyConfiguration(new DeliveryClubGroupConfiguration());
            modelBuilder.ApplyConfiguration(new DeliveryClubProductConfiguration());
        }
    }
}
