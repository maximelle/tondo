﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Models.Abstract;

namespace Tondo.Data.Configuration.Abstract
{
    public abstract class EntityBaseConfiguration<TObject> : IEntityTypeConfiguration<TObject> where TObject : EntityBase
    {
        public abstract void ConfigureImpl(EntityTypeBuilder<TObject> builder);
        public void Configure(EntityTypeBuilder<TObject> builder)
        {
            builder.Property(p => p.Id).IsRequired().HasColumnName("id");
            ConfigureImpl(builder);
        }
    }
}
