﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class YandexProductConfiguration : EntityBaseConfiguration<YandexProduct>
    {
        public override void ConfigureImpl(EntityTypeBuilder<YandexProduct> builder)
        {
            builder.ToTable("yandex-products").HasKey(p => p.Id);

            builder.Property(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.Available);
            builder.Property(p => p.Description);
            builder.Property(p => p.MenuItemId);


            builder.Property(e => e.GroupId).HasColumnName("GroupId");
            builder.HasOne(x => x.Group).WithMany(y => y.Products).HasForeignKey(d => d.GroupId);

            builder.Property(e => e.PlaceId).HasColumnName("PlaceId");
            builder.HasOne(x => x.Place).WithMany(y => y.Products).HasForeignKey(d => d.PlaceId);
        }
    }
}
