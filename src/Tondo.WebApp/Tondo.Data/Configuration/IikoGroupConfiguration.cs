﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class IikoGroupConfiguration : EntityBaseConfiguration<IikoGroup>
    {
        public override void ConfigureImpl(EntityTypeBuilder<IikoGroup> builder)
        {
            builder.ToTable("iiko-groups").HasKey(p => p.Id);

            builder.Property(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.IsDeleted);
            builder.Property(p => p.ParentGroupId);
        }
    }
}
