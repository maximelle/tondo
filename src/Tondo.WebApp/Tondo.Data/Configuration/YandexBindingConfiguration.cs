﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class YandexBindingConfiguration : IEntityTypeConfiguration<YandexBinding>
    {
        public void Configure(EntityTypeBuilder<YandexBinding> builder)
        {
            builder.ToTable("yandex-bindings").HasKey(p => p.Id);

            builder.Property(p => p.Id).IsRequired().HasColumnName("Id").ValueGeneratedOnAdd();
            builder.Property(p => p.IikoId);
            builder.Property(p => p.YandexId);
        }
    }
}
