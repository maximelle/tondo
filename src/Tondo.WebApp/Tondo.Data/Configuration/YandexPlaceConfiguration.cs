﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class YandexPlaceConfiguration :  IEntityTypeConfiguration<YandexPlace>
    {
        public void Configure(EntityTypeBuilder<YandexPlace> builder)
        {
            builder.ToTable("yandex-places");

            builder.Property(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.Available);
        }
    }
}
