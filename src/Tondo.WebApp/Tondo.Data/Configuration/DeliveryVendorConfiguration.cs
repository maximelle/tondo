﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class DeliveryVendorConfiguration : EntityBaseConfiguration<DeliveryClubVendor>
    {
        public override void ConfigureImpl(EntityTypeBuilder<DeliveryClubVendor> builder)
        {
            builder.ToTable("deliveryclub-vendors").HasKey(p => p.Id);

            builder.Property(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.VendorId);
            builder.Property(p => p.ChainId);
            builder.Property(p => p.ChainName);
        }
    }
}
