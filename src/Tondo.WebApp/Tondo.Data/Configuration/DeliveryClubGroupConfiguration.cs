﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class DeliveryClubGroupConfiguration : EntityBaseConfiguration<DeliveryClubGroup>
    {
        public override void ConfigureImpl(EntityTypeBuilder<DeliveryClubGroup> builder)
        {
            builder.ToTable("deliveryclub-groups").HasKey(p => p.Id);

            builder.Property(p => p.Id);
            builder.Property(p => p.DeliveryClubId);
            builder.Property(p => p.ExternalId);
            builder.Property(p => p.VendorId);
            builder.Property(p => p.Name);
        }
    }
}
