﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class YandexGroupConfiguration : EntityBaseConfiguration<YandexGroup>
    {
        public override void ConfigureImpl(EntityTypeBuilder<YandexGroup> builder)
        {
            builder.ToTable("yandex-groups").HasKey(p => p.Id);

            builder.Property(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.Available);
        }
    }
}
