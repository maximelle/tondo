﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class YandexOrderConfiguration : IEntityTypeConfiguration<YandexOrder>
    {
        public void Configure(EntityTypeBuilder<YandexOrder> builder)
        {
            builder.ToTable("yandex-orders").HasKey(p => p.Id);
            builder.Property(p => p.Id);
            builder.Property(p => p.Status);
        }
    }
}
