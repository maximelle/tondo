﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class DeliveryClubProductConfiguration : EntityBaseConfiguration<DeliveryClubProduct>
    {
        public override void ConfigureImpl(EntityTypeBuilder<DeliveryClubProduct> builder)
        {
            builder.ToTable("deliveryclub-products").HasKey(p => p.Id);

            builder.Property(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.DeliveryClubId);
            builder.Property(p => p.ExternalId);
            builder.Property(p => p.Type);

            builder.Property(e => e.GroupId).HasColumnName("GroupId");
            builder.HasOne(x => x.Group).WithMany(y => y.Products).HasForeignKey(d => d.GroupId).IsRequired(false);

            builder.Property(e => e.VendorId).HasColumnName("VendorId");
            builder.HasOne(x => x.Vendor).WithMany(y => y.Products).HasForeignKey(d => d.VendorId);
        }
    }
}
