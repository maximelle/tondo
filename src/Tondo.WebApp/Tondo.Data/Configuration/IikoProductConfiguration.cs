﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Tondo.Data.Configuration.Abstract;
using Tondo.Data.Models;

namespace Tondo.Data.Configuration
{
    public class IikoProductConfiguration : EntityBaseConfiguration<IikoProduct>
    {
        public override void ConfigureImpl(EntityTypeBuilder<IikoProduct> builder)
        {
            builder.ToTable("iiko-products").HasKey(p => p.Id);

            builder.Property(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.IsDeleted);
            builder.Property(p => p.Type);

            builder.HasOne(x => x.YandexProduct).WithOne(y => y.IikoProduct).HasForeignKey<YandexProduct>(p => p.IikoProductId);

            builder.Property(e => e.GroupId).HasColumnName("GroupId");
            builder.HasOne(x => x.Group).WithMany(y => y.Products).HasForeignKey(d => d.GroupId);
        }
    }
}
