﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tondo.Data.Migrations
{
    public partial class _5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "IikoProductId",
                table: "yandex-products",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Id",
                table: "yandex-places",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateTable(
                name: "iiko-groups",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    ParentGroupId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IikoGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "iiko-products",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    GroupId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IikoProduct", x => x.Id);
                    table.ForeignKey(
                        name: "FK_IikoProduct_IikoGroup_GroupId",
                        column: x => x.GroupId,
                        principalTable: "IikoGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_yandex-products_IikoProductId",
                table: "yandex-products",
                column: "IikoProductId",
                unique: true,
                filter: "[IikoProductId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_IikoProduct_GroupId",
                table: "IikoProduct",
                column: "GroupId");

            migrationBuilder.AddForeignKey(
                name: "FK_yandex-products_IikoProduct_IikoProductId",
                table: "yandex-products",
                column: "IikoProductId",
                principalTable: "IikoProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_yandex-products_IikoProduct_IikoProductId",
                table: "yandex-products");

            migrationBuilder.DropTable(
                name: "IikoProduct");

            migrationBuilder.DropTable(
                name: "IikoGroup");

            migrationBuilder.DropIndex(
                name: "IX_yandex-products_IikoProductId",
                table: "yandex-products");

            migrationBuilder.DropColumn(
                name: "IikoProductId",
                table: "yandex-products");
        }
    }
}
