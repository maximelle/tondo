﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Tondo.Data.Migrations
{
    public partial class _11 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "deliveryclub-groups",
                columns: table => new
                {
                    id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DeliveryClubId = table.Column<int>(type: "int", nullable: false),
                    ExternalId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    VendorId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_deliveryclub-groups", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "deliveryclub-products",
                columns: table => new
                {
                    id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    DeliveryClubId = table.Column<int>(type: "int", nullable: false),
                    ExternalId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IikoProductId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    VendorId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    GroupId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_deliveryclub-products", x => x.id);
                    table.ForeignKey(
                        name: "FK_deliveryclub-products_deliveryclub-groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "deliveryclub-groups",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_deliveryclub-products_deliveryclub-vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "deliveryclub-vendors",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_deliveryclub-products_iiko-products_IikoProductId",
                        column: x => x.IikoProductId,
                        principalTable: "iiko-products",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_deliveryclub-products_GroupId",
                table: "deliveryclub-products",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_deliveryclub-products_IikoProductId",
                table: "deliveryclub-products",
                column: "IikoProductId");

            migrationBuilder.CreateIndex(
                name: "IX_deliveryclub-products_VendorId",
                table: "deliveryclub-products",
                column: "VendorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "deliveryclub-products");

            migrationBuilder.DropTable(
                name: "deliveryclub-groups");
        }
    }
}
