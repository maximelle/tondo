﻿
using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Tondo.WebApp.Models
{
    public class ProductEditModel
    {
        public string Id { get; set; }
        
        [DisplayName("Наименование Яндекс еда")]
        public string Name { get; set; }

        [DisplayName("Наименование Iiko")]
        public string BindingId { get; set; }
    }


    public class ModifierEditModel
    {
        public string Id { get; set; }

        [DisplayName("Наименование Яндекс еда")]
        public string Name { get; set; }

        public List<SelectListItem> IikoModifierList { get; set; }

        [DisplayName("Модификаторы iiko")]
        public List<string> SelectedIds { get; set; }
    }
}
