﻿namespace Tondo.WebApp.Models
{
    public class Product
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
        public string PlaceName { get; set; }
        public string Type { get; set; }
        public bool IsActive { get; set; }

        public string BindingName { get; set; }
    }
}
