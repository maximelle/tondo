using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;
using Tondo.WebApp.Core;
using Tondo.WebApp.Core.Http;
using Tondo.WebApp.Sevices;
using Tondo.WebApp.Sevices.DeliveryClub;
using Tondo.WebApp.Sevices.IikoBiz;
using Tondo.WebApp.Sevices.IikoBiz.Dto;
using Tondo.WebApp.Sevices.VendorYandex;

namespace Tondo.WebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                //IikoBizService iikoBizService = new IikoBizService(new HttpClientService(), "https://iiko.biz:9900/api/0", new AuthInfo() { Name = "maximelle", Password = "_Test123" });
                //iikoBizService.Login().Wait();

                //IikoOrderRoot root = new IikoOrderRoot();


                //root.Customer = new IikoCustomer();
                //root.Customer.Name = "���� ������";
                //root.Customer.Phone = "+79170999013";
                //root.Customer.Id = "4fc0f065-269e-47fd-bca7-7b4637b4ce97";

                //root.Order = new Order();
                //root.Order.Phone = "+79170999013";
                //root.Order.Id = Guid.NewGuid().ToString();
                //root.Order.Date = DateTime.Now.ToString("yyyy-MM-dd 17:mm:ss");

                //root.Order.Address = new Address()
                //{
                //    City = "������",
                //    Street = "���������� �������",
                //    Home = "1",
                //    Apartment = "1",
                //    Comment = "��������� �������"
                //};

                //root.Order.Items = new List<Item>();

                //Item item = new Item()
                //    {
                //        Id = "a4c376f0-275a-4f6c-8a30-31504420b8c3",
                //         Amount = 2,
                //         Modifiers = new List<OrderModifier>()
                //         {
                //             new OrderModifier(){ Id = "c840d6cb-fb03-49f6-baa6-211adc93d708", Amount = 2},
                //             new OrderModifier(){ Id = "a0877bd0-3145-468e-84f3-c08d0ca36b77", Amount = 2, GroupId = "96705951-2b50-46ab-8a5a-f0dffb47a3d4"},
                //         }
                //};

                //root.Order.Items.Add(item);
                //var result = iikoBizService.CreateOrder(root).Result;

                //&request_timeout = 00%3A01%3A00

                //IikoBizService iikoBizService = new IikoBizService(new HttpClientService(), "https://iiko.biz:9900/api/0", new AuthInfo() { Name = "FKapi", Password = "4aSHasNV" });
                //iikoBizService.Login().Wait();

                //var items = iikoBizService.GetOrganization().Result;
                //////var nomenclature = iikoBizService.GetNomenclature(items.First().Id).Result;

                ////var pt = iikoBizService.GetPaymentTypes(items.First().Id).Result;

                //var pt = iikoBizService.GetOrderTypes(items.First().Id).Result;

                //var service = new VendorYandexService(new HttpClientService(), "https://vendor.eda.yandex/api/v1", new AuthInfo() { Name = "tondomoscow@gmail.com", Password = "p4k2PSjDFG" });
                //service.Login().Wait();

                //var orders = service.GetOrders().Result;

                //var items = service.GetRestaurants().Result;
                //foreach (var r in items)
                //{
                //    var menu = service.GetMenu(r.Id).Result;
                //}

                //var del = new DeliveryClubService(new HttpClientService(), "https://restaurant.delivery-club.ru",
                //    new AuthInfo() { Name = "tondomoscow@gmail.com", Password = "hymc1EnsrmYC" });
                //del.Login().Wait();

                ////var dd = del.GetVendorList().Result;

                //var rootOrder = del.GetOrders(DateTime.Now.AddDays(-1));

                //foreach (var order in rootOrder.Result.items)
                //{
                //    var orderItems = del.GetOrderProducts(order.id).Result;
                //}

                ////https://docs.google.com/document/d/1pRQNIn46GH1LVqzBUY5TdIIUuSCOl-A_xeCBbogd2bE/edit

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
