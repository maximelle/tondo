using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml;
using AutoMapper;
using Hangfire;
using Hangfire.SqlServer;
using Hangfire.States;
using log4net;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using Tondo.Data;
using Tondo.WebApp.Core;
using Tondo.WebApp.Core.Http;
using Tondo.WebApp.Mapping;
using Tondo.WebApp.Sevices;
using Tondo.WebApp.Sevices.DeliveryClub;
using Tondo.WebApp.Sevices.IikoBiz;
using Tondo.WebApp.Sevices.VendorYandex;

namespace Tondo.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new YandexModelProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton<IMapper>(mapper);

            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<TodoDataContext>(options => options.UseSqlServer(connection));

            services.AddSingleton<VendorYandexService>(YandexImplementationFactory);
            services.AddSingleton<IikoBizService>(IikoBizImplementationFactory);
            services.AddSingleton<DeliveryClubService>(DeliveryClubServiceImplementationFactory);
            services.AddScoped<VendorYandexOrderWorker>();
            services.AddScoped<DeliveryClubOrderWorker>();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();

            // Add Hangfire services.
            //services.AddHangfire(configuration => configuration
            //    .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
            //    .UseSimpleAssemblyNameTypeSerializer()
            //    .UseRecommendedSerializerSettings()
            //    .UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection"), new SqlServerStorageOptions
            //    {
            //        CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
            //        SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
            //        QueuePollInterval = TimeSpan.Zero,
            //        UseRecommendedIsolationLevel = true,
            //        DisableGlobalLocks = true
            //    }));



            // Add the processing server as IHostedService
            //services.AddHangfireServer();

            // Add framework services.
            services.AddMvc();
        }

        private VendorYandexService YandexImplementationFactory(IServiceProvider arg)
        {
            var service = new VendorYandexService(new HttpClientService(), "https://vendor.eda.yandex/api/v1", new AuthInfo() { Name = "tondomoscow@gmail.com", Password = "p4k2PSjDFG" });
            service.Login().Wait();
            return service;
        }

        private DeliveryClubService DeliveryClubServiceImplementationFactory(IServiceProvider arg)
        {
            var del = new DeliveryClubService(new HttpClientService(), "https://restaurant.delivery-club.ru",
                new AuthInfo() { Name = "tondomoscow@gmail.com", Password = "hymc1EnsrmYC" });
            del.Login().Wait();
            return del;
        }

        private IikoBizService IikoBizImplementationFactory(IServiceProvider arg)
        {
            var service = new IikoBizService(new HttpClientService(), "https://iiko.biz:9900/api/0", new AuthInfo() { Name = "FKapi", Password = "4aSHasNV" });
            service.Login().Wait();
            return service;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env/*, IBackgroundJobClient backgroundJobClient*/, ILoggerFactory loggerFactory)
        {
            SetLog4NetConfiguration();

            loggerFactory.AddLog4Net();
            

            ILog log = LogManager.GetLogger(typeof(Startup));
            log.Info("Hi fi");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                //app.UseHsts();
            }
            //app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            //app.UseHangfireDashboard("/hangfire");
            //var yandexService = app.ApplicationServices.GetService(typeof(VendorYandexService)) as VendorYandexService;
            //var deliveryClubService = app.ApplicationServices.GetService(typeof(DeliveryClubService)) as DeliveryClubService;

            //RecurringJob.AddOrUpdate<VendorYandexOrderWorker>(x =>
            //    x.Run(), "*/59 * * * * *");

            //RecurringJob.AddOrUpdate<DeliveryClubOrderWorker>(x =>
            //    x.Run(), "*/59 * * * * *");

            //backgroundJobClient.Create(() => yandexService.GetOrders(), new ScheduledState(TimeSpan.FromSeconds(5)));
            var deliveryClubService = app.ApplicationServices.GetService(typeof(DeliveryClubService)) as DeliveryClubService;
            var iikoBizService = app.ApplicationServices.GetService(typeof(IikoBizService)) as IikoBizService;

            var serviceScopeFactory = (IServiceScopeFactory)app.ApplicationServices.GetService(typeof(IServiceScopeFactory));
            TodoDataContext dbContext;

            using (var scope = serviceScopeFactory.CreateScope())
            {
                var services = scope.ServiceProvider;

                 dbContext = services.GetRequiredService<TodoDataContext>();
            }

            //TodoDataContext dbContext = app.ApplicationServices.GetRequiredService(typeof(TodoDataContext)) as TodoDataContext;

            DeliveryClubOrderWorker worker = new DeliveryClubOrderWorker(deliveryClubService,  dbContext, iikoBizService);


            //DeliveryClubOrderWorker worker = app.ApplicationServices.GetService(typeof(DeliveryClubOrderWorker)) as DeliveryClubOrderWorker;
            worker.Run().Wait();
        }

        private static readonly string LOG_CONFIG_FILE = @"log4net.config";

        private static void SetLog4NetConfiguration()
        {
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead(LOG_CONFIG_FILE));

            var repo = LogManager.CreateRepository(Assembly.GetEntryAssembly(), typeof(log4net.Repository.Hierarchy.Hierarchy));

            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }
    }
}
