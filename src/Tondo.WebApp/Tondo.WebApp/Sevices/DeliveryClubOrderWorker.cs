﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tondo.Data;
using Tondo.WebApp.Sevices.DeliveryClub;
using Tondo.WebApp.Sevices.IikoBiz;
using Tondo.WebApp.Sevices.IikoBiz.Dto;
using DeliveryOrder = Tondo.WebApp.Sevices.DeliveryClub.Dto.Item;
using IikoOrder = Tondo.WebApp.Sevices.IikoBiz.Dto.Order;

namespace Tondo.WebApp.Sevices
{
    public class DeliveryClubOrderWorker : OrderWorkerBase
    {
        private readonly DeliveryClubService _service;

        public DeliveryClubOrderWorker(DeliveryClubService service, TodoDataContext dataContext, IikoBizService iikoBizService) : base(dataContext, iikoBizService)
        {
            _service = service;
        }

        public override async Task Run()
        {
            if (GetBusy())
            {
                return;
            }

            SetBusy(true);

            List<DeliveryOrder> orders;

            try
            {
                var iikoNomenclature = await IikoBizService.GetNomenclature();
                IikoNomenclature = iikoNomenclature;

                var org = await IikoBizService.GetOrganization();

                PaymentItems = await IikoBizService.GetPaymentTypes(org.First().Id);
                OrderTypes = await IikoBizService.GetOrderTypes(org.First().Id);

                orders =  _service.GetOrders(DateTime.Now.AddDays(-1)).Result.items.ToList();
            }
            catch (UnauthorizedAccessException exception)
            {
                Logger.Error(exception);
                await IikoBizService.Login();
                SetBusy(false);
                return;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                await IikoBizService.Login();
                SetBusy(false);
                return;
            }

            foreach (var o in orders)
            {
                await ProcessingOrder(o);
            }

            SetBusy(false);
        }

        private async Task ProcessingOrder(DeliveryOrder order)
        {
            try
            {
                if (order.statusCode != 63)
                {
                    Logger.Info($"Заказа {order.id}/{order.createdAt} не принятый, статус: {order.statusCode}");
                    return;
                }

                var storageOrder = Context.YandexOrders.FirstOrDefault(x => x.Id == order.id.ToString() && x.Status == order.statusCode.ToString());
                if (storageOrder != null)
                {
                    Logger.Info($"Заказ {order.id}/{order.createdAt} уже обработан");
                    return;
                }

                //address=Москва, улица Косыгина, 2к2, под. 1, этаж 4, кв./офис 27
                //phoneNumber = +74951183830 доб. 0201

                IikoOrderRoot orderRoot = new IikoOrderRoot();

                string phone = String.Empty;
                StringBuilder sb = new StringBuilder();

                if (order.customer != null)
                {
                    orderRoot.Customer = new IikoCustomer();
                    orderRoot.Customer.Name = order.customer.lastName + order.customer.firstName;
                    phone = order.customer.mobileCountryCode + order.customer.mobileNumber;
                    phone = phone.Replace("(", string.Empty).Replace(")", string.Empty);
                    orderRoot.Customer.Phone = phone;
                    orderRoot.Customer.Id = Guid.NewGuid().ToString();
                }

                Logger.Info($"Клиент {orderRoot.Customer.Id}/{orderRoot.Customer.Name}/{orderRoot.Customer.Phone}");

                IikoOrder iikoOrder = new Order();
                iikoOrder.Id = Guid.NewGuid().ToString();
                iikoOrder.Phone = phone; 
                iikoOrder.Date = order.createdAt.AddHours(-4).ToString("yyyy-MM-dd HH:mm:ss");

                if (order.customerAddress != null)
                {
                    iikoOrder.Address = new Address()
                    {
                        City = order.customerAddress.cityName,
                        Street = order.customerAddress.street.Replace("улица", "").Replace("ул.", ""),
                        Home = order.customerAddress.building,
                        Apartment = order.customerAddress.flatNumber,
                        Floor = order.customerAddress.floor,
                        Entrance = order.customerAddress.entrance,
                        DoorPhone = order.customerAddress.intercom,
                        Comment = order.customerAddress.formattedCustomerAddress
                    };

                }


                iikoOrder.Items = new List<Item>();

                var rootProducts = await _service.GetOrderProducts(order.id);

                var ddd = "";

                //foreach (var item in rootProducts.items)
                //{
                //    Logger.Info($"Сопоставление позиции Id: {item.id}, name : {item.name}");

                //    var product = Context.DeliveryClubProducts.FirstOrDefault(x => x.DeliveryClubId == item.id);

                //    if (product != null)
                //    {
                //        var iikoProduct = Context.IikoProducts.FirstOrDefault(x => x.Id == product.IikoProductId);

                //        if (iikoProduct != null)
                //        {
                //            Logger.Info($"Сопоставление найдено Id: {iikoProduct.Id}, name : {iikoProduct.Name}");
                //            Item iikoItem = new Item();
                //            iikoItem.Id = iikoProduct.Id;
                //            iikoItem.Amount = item.quantity;
                //            iikoItem.Sum = item.totalPrice;
                //            iikoItem.Modifiers = new List<OrderModifier>();

                //            foreach (var option in item.Options)
                //            {

                //                OrderModifier mod = null;

                //                Logger.Info($"Сопоставление модификатора menuItemOptionId: {option.MenuItemOptionId}, name : {option.Name} group : {option.GroupName}");

                //                var modifier = Context.YandexProducts.FirstOrDefault(x => x.Type == "modifier" && x.MenuItemId == option.MenuItemOptionId);
                //                if (modifier != null)
                //                {
                //                    var bindings = Context.YandexBindings.Where(x => x.YandexId == modifier.Id).ToList();

                //                    var iikoNomenclatureProduct =
                //                        IikoNomenclature.Products.FirstOrDefault(x => x.Id == iikoItem.Id);


                //                    var iikoNomenclatureGroupModifiers = iikoNomenclatureProduct.GroupModifiers;
                //                    //var iikoNomenclatureModifiers = iikoNomenclatureProduct.Modifiers;

                //                    foreach (var gm in iikoNomenclatureGroupModifiers)
                //                    {
                //                        foreach (var childGm in gm.ChildModifiers)
                //                        {
                //                            var binding = bindings.FirstOrDefault(x => x.IikoId == childGm.ModifierId);
                //                            if (binding != null)
                //                            {
                //                                var iikoGroupMod =
                //                                    IikoNomenclature.Products.FirstOrDefault(x => x.Id == binding.IikoId);

                //                                Logger.Info($"Сопоставление групового модификатора найдено id: {iikoGroupMod.Id}, name : {iikoGroupMod.Name} group : {iikoGroupMod.GroupId}");

                //                                mod = new OrderModifier()
                //                                {
                //                                    Id = binding.IikoId,
                //                                    Amount = option.Quantity,
                //                                    GroupId = iikoGroupMod.GroupId
                //                                };

                //                                break;
                //                            }
                //                        }

                //                        if (mod != null)
                //                        {
                //                            break;
                //                        }
                //                    }
                //                }
                //                else
                //                {
                //                    throw new Exception($"Модификатор MenuItemOptionId: {option.MenuItemOptionId}, name : {option.Name} не найден в БД");
                //                }

                //                iikoItem.Modifiers.Add(mod);
                //            }

                //            iikoOrder.Items.Add(iikoItem);
                //        }
                //        else
                //        {
                //            throw new Exception($"Не найдено сопоставление для  блюда MenuItemId: {item.MenuItemId}, name : {item.Name} не найдено в БД");
                //        }
                //    }
                //    else
                //    {
                //        throw new Exception($"Блюдо MenuItemId: {item.MenuItemId}, name : {item.Name} не найдено в БД");
                //    }

                //}

                if (order.paymentType.paymentCode == "card_online") 
                {

                    var pt = PaymentItems.FirstOrDefault(x => x.Code == "YAEDA");
                    if (pt != null)
                    {
                        iikoOrder.PaymentItems = new List<PaymentItem>();
                        PaymentItem item = new PaymentItem();
                        item.PaymentType = pt;
                        item.IsProcessedExternally = true;
                        item.IsPreliminary = false;
                        item.isExternal = true;
                        item.Sum = order.totalValue;
                        iikoOrder.PaymentItems.Add(item);
                    }
                }

                //DELIVERY_BY_COURIER
                //iikoOrder.OrderTypeId
                var orderType = OrderTypes.FirstOrDefault(x => x.OrderServiceType == "DELIVERY_BY_COURIER");
                if (orderType != null)
                {
                    iikoOrder.OrderTypeId = orderType.Id;
                }

                iikoOrder.Comment = order.customerAddress.deliveryInstructions + order.paymentType.paymentCode + "-" + order.paymentType.paymentCode;
                orderRoot.Order = iikoOrder;
                //var response = await IikoBizService.CreateOrder(orderRoot);

                //Data.Models.YandexOrder o = new Data.Models.YandexOrder()
                //{
                //    Id = iikoOrder.Id,
                //    Status = order.statusCode.ToString()
                //};
                //Context.YandexOrders.Add(o);
                await Context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Logger.Error("Ошибка обрабоки заказа", e);
            }
        }
    }
}
