﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Tondo.WebApp.Core;
using Tondo.WebApp.Core.Http;
using Tondo.WebApp.Sevices.VendorYandex.Dto;

namespace Tondo.WebApp.Sevices.VendorYandex
{
    public class VendorYandexService : ClientServiceBase
    {
        private string _token;
        
        public VendorYandexService(IHttpClientService httpService, string baseUrl, AuthInfo authInfo) : base(httpService, baseUrl, authInfo)
        {
            
        }

        public override async Task Login()
        {
            try
            {
               var result = await HttpService.PostAsync<Response<LoginResponse>, LoginRequest>($"{BaseUrl}/client/login", new LoginRequest(){Email = AuthInfo.Name, Password = AuthInfo.Password},  null);
                _token = result.Payload.Token;

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<Restaurant>> GetRestaurants()
        {
            try
            {
                var result = await HttpService.GetAsync<ResponseWithMeta<List<Restaurant>, ListMeta>>($"{BaseUrl}/client/restaurants?limit=999", GetRequestHeaders());
                return result.Payload;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<MenuResponse> GetMenu(int organizationId)
        {
            try
            {
                var result = await HttpService.GetAsync<Response<MenuResponse>>($"{BaseUrl}/client/place/{organizationId}/menu", GetRequestHeaders());
                return result.Payload;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<Order>> GetOrders()
        {
            try
            {

                ServicePointManager.ServerCertificateValidationCallback =
                    delegate (object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    {
                        return true;
                    };

                


                ////"2021-01-19T07:00:00+03:00"
                var headers = GetRequestHeaders();
                headers.Add("cache-control", "no-cache");
                headers.Add("X-App-Version", "5.15.3");
                headers.Add("Accept", "*/*");
                headers.Add("accept-encoding", "gzip, deflate");
                headers.Add("User-Agent", "PostmanRuntime/6.1.6");
                SearchRequest request = new SearchRequest(){From = DateTime.Now.Date.ToString("yyyy-MM-ddT00:00:00+03:00") };

                var result = await HttpService.PostAsync<ResponseWithMeta<List<Order>, ListMeta>, SearchRequest>($"{BaseUrl}/client/orders/search?limit=30", request, headers);
                return result.Payload;

                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private Dictionary<string, string> GetRequestHeaders()
        {
            return  new Dictionary<string, string>()
            {
                {"X-Token", _token }
            };
        }
    }
}
