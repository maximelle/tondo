﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class Modifier
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("minAmount")]
        public int minAmount { get; set; }

        [JsonProperty("maxAmount")]
        public int MaxAmount  { get; set; }

        [JsonProperty("available")]
        public bool Available { get; set; }

        [JsonProperty("menuItemOptionId")]
        public int MenuItemOptionId { get; set; }
    }
}
