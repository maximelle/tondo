﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class Response<T>
    {
        [JsonProperty("isSuccess")]
        public bool isSuccess { get; set; }

        [JsonProperty("payload")]
        public T Payload { get; set; }
    }


    [JsonObject]
    public class ResponseWithMeta<T, K>
    {
        [JsonProperty("isSuccess")]
        public bool isSuccess { get; set; }
        
        [JsonProperty("payload")]
        public T Payload { get; set; }

        [JsonProperty("meta")]
        public K Meta { get; set; }
    }

    [JsonObject]
    public class ListMeta
    {
        [JsonProperty("count")]
        public int Count { get; set; } 
    }
}
