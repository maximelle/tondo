﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class GroupModifier
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("modifiers")]
        public List<Modifier> Modifiers { get; set; }

        [JsonProperty("minSelectedModifiers")]
        public int MinSelectedModifiers { get; set; }

        [JsonProperty("maxSelectedModifiers")]
        public int MaxSelectedModifiers { get; set; }

        [JsonProperty("menuItemOptionGroupId")]
        public int MenuItemOptionGroupId { get; set; }
    }
}
