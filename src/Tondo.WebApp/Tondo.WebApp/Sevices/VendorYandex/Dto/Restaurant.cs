﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class Restaurant
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isAvailable")]
        public bool IsAvailable { get; set; }

        [JsonProperty("isSwitchingOnRequested")]
        public bool IsSwitchingOnRequested { get; set; }

        [JsonProperty("disableDetails")]
        public Disabledetails DisableDetails { get; set; }

        [JsonProperty("showShippingTime")]
        public bool ShowShippingTime { get; set; }

        [JsonProperty("integrationType")]
        public string IntegrationType { get; set; }

        [JsonProperty("currency")]
        public Currency Currency { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    [JsonObject]
    public class Disabledetails
    {
        [JsonProperty("disabledAt")]
        public DateTime DisabledAt { get; set; }

        [JsonProperty("availableAt")]
        public DateTime AvailableAt { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("reason")]
        public int Reason { get; set; }

        [JsonProperty("canBeEnabled")]
        public bool CanBeEnabled { get; set; }
    }

    [JsonObject]
    public class Currency
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("sign")]
        public string Sign { get; set; }

        [JsonProperty("decimalPlaces")]
        public int DecimalPlaces { get; set; }
    }

}
