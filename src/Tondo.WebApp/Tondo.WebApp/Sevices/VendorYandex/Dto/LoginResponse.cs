﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class LoginResponse
    {
        [JsonProperty("token")]
        public string Token { get; set; }
    }
}
