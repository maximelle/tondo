﻿using System;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class SearchRequest
    {
        [JsonProperty("from")]
        public string From { get; set; }
    }
}
