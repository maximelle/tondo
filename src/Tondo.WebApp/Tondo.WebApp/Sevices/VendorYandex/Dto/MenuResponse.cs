﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class MenuResponse
    {
        [JsonProperty("lastChange")]
        public DateTime LastChange { get; set; }

        [JsonProperty("categories")]
        public List<Group> Groups { get; set; }

        [JsonProperty("items")]
        public List<Product> Items { get; set; }
    }
}
