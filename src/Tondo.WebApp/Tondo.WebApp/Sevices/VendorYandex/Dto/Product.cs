﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class Product
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("categoryId")]
        public string GroupId { get; set; }

        [JsonProperty("measure")]
        public string Measure { get; set; }

        [JsonProperty("measureUnit")]
        public string MeasureUnit { get; set; }

        [JsonProperty("available")]
        public bool Available { get; set; }

        [JsonProperty("modifierGroups")]
        public List<GroupModifier> GroupModifiers { get; set; }

        [JsonProperty("menuItemId")]
        public int MenuItemId { get; set; }
    }
}
