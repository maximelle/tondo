﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.VendorYandex.Dto
{
    [JsonObject]
    public class Order
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("externalId")]
        public string ExternalId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("restaurantConfirmedAt")]
        public DateTime RestaurantConfirmedAt { get; set; }

        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }

        [JsonProperty("sum")]
        public int Sum { get; set; }

        [JsonProperty("detailSum")]
        public Detailsum DetailSum { get; set; }

        [JsonProperty("countPersons")]
        public int CountPersons { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("deliveryStatus")]
        public string DeliveryStatus { get; set; }

        [JsonProperty("paymentType")]
        public string PaymentType { get; set; }

        [JsonProperty("changeOn")]
        public object ChangeOn { get; set; }

        [JsonProperty("timeToDelivery")]
        public int TimeToDelivery { get; set; }

        [JsonProperty("restaurant")]
        public RestaurantShort Restaurant { get; set; }

        [JsonProperty("user")]
        public User User { get; set; }

        [JsonProperty("items")]
        public List<OrderItem> Items { get; set; }

        [JsonProperty("courier")]
        public Courier Courier { get; set; }

        [JsonProperty("promoInfos")]
        public string[] PromoInfos { get; set; }

        [JsonProperty("totalDiscount")]
        public int TotalDiscount { get; set; }

        [JsonProperty("isReimbursementToPlace")]
        public string IsReimbursementToPlace { get; set; }

        [JsonProperty("pickup")]
        public bool Pickup { get; set; }

        [JsonProperty("changeInitializedAt")]
        public string ChangeInitializedAt { get; set; }

        [JsonProperty("lastUpdatedAt")]
        public DateTime LastUpdatedAt { get; set; }

        [JsonProperty("onHold")]
        public bool OnHold { get; set; }

        [JsonProperty("changeId")]
        public string ChangeId { get; set; }

        [JsonProperty("costIncreaseAllowed")]
        public bool CostIncreaseAllowed { get; set; }
    }

    [JsonObject]
    public class Detailsum
    {
        [JsonProperty("itemsCost")]
        public int ItemsCost { get; set; }

        [JsonProperty("deliveryCost")]
        public int DeliveryCost { get; set; }
    }

    [JsonObject]
    public class RestaurantShort
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    [JsonObject]
    public class User
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phoneNumber")]
        public string PhoneNumber { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("addressComment")]
        public string AddressComment { get; set; }

        [JsonProperty("extendedAddress")]
        public Extendedaddress ExtendedAddress { get; set; }
    }

    [JsonObject]
    public class Extendedaddress
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("house")]
        public string House { get; set; }

        [JsonProperty("entrance")]
        public string Entrance { get; set; }

        [JsonProperty("floor")]
        public string Floor { get; set; }

        [JsonProperty("office")]
        public string Office { get; set; }

        [JsonProperty("plot")]
        public string Plot { get; set; }

        [JsonProperty("building")]
        public string Building { get; set; }

        [JsonProperty("doorcode")]
        public string Doorcode { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }
    }

    [JsonObject]
    public class Courier
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        [JsonProperty("approximateArrivalTime")]
        public string ApproximateArrivalTime { get; set; }

        [JsonProperty("metaInfo")]
        public Metainfo MetaInfo { get; set; }
    }

    [JsonObject]
    public class Metainfo
    {
        [JsonProperty("isRover")]
        public bool IsRover { get; set; }

        [JsonProperty("isHardOfHearing")]
        public bool IsHardOfHearing { get; set; }
    }

    [JsonObject]
    public class OrderItem
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("categoryId")]
        public int CategoryId { get; set; }

        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }

        [JsonProperty("weight")]
        public string Weight { get; set; }

        [JsonProperty("options")]
        public List<Option> Options { get; set; }

        [JsonProperty("promoInfos")]
        public object[] PromoInfos { get; set; }

        [JsonProperty("menuItemId")]
        public int MenuItemId { get; set; }
    }


    [JsonObject]
    public class Option
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("sum")]
        public int Sum { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("groupId")]
        public int GroupId { get; set; }

        [JsonProperty("groupName")]
        public string GroupName { get; set; }

        [JsonProperty("promoInfos")]
        public object[] PromoInfos { get; set; }

        [JsonProperty("menuItemOptionId")]
        public int MenuItemOptionId { get; set; }
    }




}
