﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using Tondo.Data;
using Tondo.WebApp.Sevices.IikoBiz;
using Tondo.WebApp.Sevices.IikoBiz.Dto;
using Tondo.WebApp.Sevices.VendorYandex;

using YandexOrder =  Tondo.WebApp.Sevices.VendorYandex.Dto.Order;
using IikoOrder = Tondo.WebApp.Sevices.IikoBiz.Dto.Order;

namespace Tondo.WebApp.Sevices
{
    public class VendorYandexOrderWorker : OrderWorkerBase
    {
        private readonly VendorYandexService _yandexService;

        public VendorYandexOrderWorker(VendorYandexService yandexService, IikoBizService iikoBizService, TodoDataContext context) : base(context, iikoBizService)
        {
            _yandexService = yandexService;
        }

        public override async Task Run()
        {
            if (GetBusy())
            {
                return;
            }

            SetBusy(true);

            List<YandexOrder> orders;

            try
            {
                var iikoNomenclature = await IikoBizService.GetNomenclature();
                IikoNomenclature = iikoNomenclature;

                var org = await IikoBizService.GetOrganization();

                PaymentItems = await IikoBizService.GetPaymentTypes(org.First().Id);
                OrderTypes = await IikoBizService.GetOrderTypes(org.First().Id);

                orders = await _yandexService.GetOrders();
            }
            catch (UnauthorizedAccessException exception)
            {
                Logger.Error(exception);
                await IikoBizService.Login();
                SetBusy(false);
                return;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                await IikoBizService.Login();
                SetBusy(false);
                return;
            }

            foreach (var o in orders)
            {
                await ProcessingOrder(o);
            }

            SetBusy(false);

        }

        private async Task ProcessingOrder(YandexOrder order)
        {
            try
            {
                if (order.Status != "accepted")
                {
                    Logger.Info($"Заказа {order.Id}/{order.CreatedAt} не принятый, статус: {order.Status}");
                    return;
                }

                var storageOrder = Context.YandexOrders.FirstOrDefault(x => x.Id == order.Id && x.Status == order.Status);
                if (storageOrder != null)
                {
                    Logger.Info($"Заказ {order.Id}/{order.CreatedAt} уже обработан");
                    return;
                }

                //address=Москва, улица Косыгина, 2к2, под. 1, этаж 4, кв./офис 27
                //phoneNumber = +74951183830 доб. 0201

                IikoOrderRoot orderRoot = new IikoOrderRoot();

                string phone = String.Empty;
                StringBuilder sb = new StringBuilder();

                if (order.User != null)
                {
                    if (string.IsNullOrEmpty(order.User.PhoneNumber) == false)
                    {
                        string str = order.User.PhoneNumber;
                        //return new string(input.Where(c => char.IsDigit(c)).ToArray());
                        foreach (char ch in str)
                        {
                            if (ch == '+')
                            {
                                continue;
                            }

                            if (char.IsDigit(ch) == false)
                            {
                                break;
                            }

                            sb.Append(ch);
                        }
                    }

                    phone = sb.ToString();

                    orderRoot.Customer = new IikoCustomer();
                    orderRoot.Customer.Name = order.User.Name;
                    orderRoot.Customer.Phone = phone;//order.User.PhoneNumber;
                    orderRoot.Customer.Id = Guid.NewGuid().ToString();

                    Logger.Info($"Клиент {orderRoot.Customer.Id}/{orderRoot.Customer.Name}/{orderRoot.Customer.Phone}");
                }

                IikoOrder iikoOrder = new Order();
                iikoOrder.Id = order.Id;
                iikoOrder.Phone = phone; //orderRoot.Customer.Phone;
                //"date": "2021-01-21 16:07:00",
                iikoOrder.Date = order.EndDate.AddHours(-4).ToString("yyyy-MM-dd HH:mm:ss");
                iikoOrder.Address = new Address()
                {
                    City = order.User.ExtendedAddress.City,
                    Street = order.User.ExtendedAddress.Street.Replace("улица", "").Replace("ул.", ""),
                    Home = order.User.ExtendedAddress.House,
                    Apartment = order.User.ExtendedAddress.Office,
                    Floor = order.User.ExtendedAddress.Floor,
                    Entrance = order.User.ExtendedAddress.Entrance,
                    DoorPhone = order.User.ExtendedAddress.Doorcode,
                    Comment = order.User.Address + "/" + order.User.AddressComment
                };
                
                iikoOrder.Items = new List<Item>();

                foreach (var item in order.Items)
                {
                    Logger.Info($"Сопоставление позиции Id: {item.Id}, name : {item.Name}");

                    var product = Context.YandexProducts.FirstOrDefault(x => x.MenuItemId == item.MenuItemId);

                    if (product != null)
                    {
                        var iikoProduct = Context.IikoProducts.FirstOrDefault(x => x.Id == product.IikoProductId);

                        if (iikoProduct != null)
                        {
                            Logger.Info($"Сопоставление найдено Id: {iikoProduct.Id}, name : {iikoProduct.Name}");
                            Item iikoItem = new Item();
                            iikoItem.Id = iikoProduct.Id;
                            iikoItem.Amount = item.Quantity;
                            iikoItem.Sum = item.Sum;
                            iikoItem.Modifiers = new List<OrderModifier>();

                            foreach (var option in item.Options)
                            {

                                OrderModifier mod = null;

                                Logger.Info($"Сопоставление модификатора menuItemOptionId: {option.MenuItemOptionId}, name : {option.Name} group : {option.GroupName}");

                                var modifier = Context.YandexProducts.FirstOrDefault(x => x.Type =="modifier" && x.MenuItemId == option.MenuItemOptionId);
                                if (modifier != null)
                                {
                                    var bindings = Context.YandexBindings.Where(x => x.YandexId == modifier.Id).ToList();

                                    var iikoNomenclatureProduct =
                                        IikoNomenclature.Products.FirstOrDefault(x => x.Id == iikoItem.Id);


                                    var iikoNomenclatureGroupModifiers = iikoNomenclatureProduct.GroupModifiers;
                                    //var iikoNomenclatureModifiers = iikoNomenclatureProduct.Modifiers;

                                    foreach (var gm in iikoNomenclatureGroupModifiers)
                                    {
                                        foreach (var childGm in gm.ChildModifiers)
                                        {
                                            var binding = bindings.FirstOrDefault(x => x.IikoId == childGm.ModifierId);
                                            if (binding != null)
                                            {
                                                var iikoGroupMod =
                                                    IikoNomenclature.Products.FirstOrDefault(x => x.Id == binding.IikoId);

                                                Logger.Info($"Сопоставление групового модификатора найдено id: {iikoGroupMod.Id}, name : {iikoGroupMod.Name} group : {iikoGroupMod.GroupId}");

                                                mod = new OrderModifier()
                                                {
                                                    Id = binding.IikoId,
                                                    Amount = option.Quantity,
                                                    GroupId = iikoGroupMod.GroupId
                                                };

                                                break;
                                            }
                                        }

                                        if (mod != null)
                                        {
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception($"Модификатор MenuItemOptionId: {option.MenuItemOptionId}, name : {option.Name} не найден в БД");
                                }

                                iikoItem.Modifiers.Add(mod);
                            }

                            iikoOrder.Items.Add(iikoItem);
                        }
                        else
                        {
                            throw new Exception($"Не найдено сопоставление для  блюда MenuItemId: {item.MenuItemId}, name : {item.Name} не найдено в БД");
                        }
                    }
                    else
                    {
                        throw new  Exception($"Блюдо MenuItemId: {item.MenuItemId}, name : {item.Name} не найдено в БД");
                    }

                }

                if (order.PaymentType == "cashless")
                {
                    
                    var pt = PaymentItems.FirstOrDefault(x => x.Code == "YAEDA");
                    if (pt != null)
                    {
                        iikoOrder.PaymentItems = new List<PaymentItem>();
                        PaymentItem item = new PaymentItem();
                        item.PaymentType = pt;
                        item.IsProcessedExternally = true;
                        item.IsPreliminary = false;
                        item.isExternal = true;
                        item.Sum = order.Sum;
                        iikoOrder.PaymentItems.Add(item);
                    }
                }

                //DELIVERY_BY_COURIER
                //iikoOrder.OrderTypeId
                var orderType = OrderTypes.FirstOrDefault(x => x.OrderServiceType == "DELIVERY_BY_COURIER");
                if (orderType != null)
                {
                    iikoOrder.OrderTypeId = orderType.Id;
                }

                iikoOrder.Comment = order.Comment;
                orderRoot.Order = iikoOrder;
                var response = await IikoBizService.CreateOrder(orderRoot);

                Data.Models.YandexOrder o = new Data.Models.YandexOrder()
                {
                    Id = iikoOrder.Id,
                    Status = order.Status
                };
                Context.YandexOrders.Add(o);
                await Context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Logger.Error("Ошибка обрабоки заказа", e);
            }
        }
    }
}
