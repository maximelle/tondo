﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class Error
    {
        [JsonProperty]
        public string Code { get; set; }

        [JsonProperty]
        public string Message { get; set; }

        [JsonProperty]
        public string Description { get; set; }

        [JsonProperty]
        public int HttpStatusCode { get; set; }

        [JsonProperty]
        public string UiMessage { get; set; }
    }
}
