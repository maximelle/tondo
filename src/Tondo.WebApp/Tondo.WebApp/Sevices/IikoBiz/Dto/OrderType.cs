﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class RootOrderTypes
    {
        [JsonProperty]
        public List<OrderType> Items { get; set; }
    }

    [JsonObject]
    public class OrderType
    {
        [JsonProperty]
        public string Id { get; set; }

        [JsonProperty]
        public string Name { get; set; }

        [JsonProperty]
        public string OrderServiceType { get; set; }
    }
}
