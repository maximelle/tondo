﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class Modifier
    {
        [JsonProperty("maxAmount")]
        public int MaxAmount { get; set; }

        [JsonProperty("minAmount")]
        public int MinAmount { get; set; }

        [JsonProperty("modifierId")]
        public string ModifierId { get; set; }

        [JsonProperty("required")]
        public bool Required { get; set; }

        [JsonProperty("defaultAmount")]
        public int DefaultAmount { get; set; }
    }
}
