﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class OrderResponse
    {
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
    }
}
