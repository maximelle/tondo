﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class Product
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("parentGroup")]
        public string ParentGroupId { get; set; }

        [JsonProperty("groupId")]
        public string GroupId { get; set; }

        [JsonProperty("modifiers")]
        public List<Modifier> Modifiers { get; set; }

        [JsonProperty("groupModifiers")]
        public List<GroupModifier> GroupModifiers { get; set; }
    }
}
