﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class RootPaymentTypes
    {
        [JsonProperty("paymentTypes")]
        public List<PaymentType> PaymentTypes { get; set; }
    }

    [JsonObject]
    public class PaymentType
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        //[JsonProperty]
        //public string comment { get; set; }

        //[JsonProperty]
        //public bool combinable { get; set; }

        //[JsonProperty]
        //public int externalRevision { get; set; }
        //public object applicableMarketingCampaigns { get; set; }
        [JsonProperty("deleted")]
        public bool Deleted { get; set; }
    }
}
