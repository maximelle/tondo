﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class Group
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("isIncludedInMenu")]
        public bool IsIncludedInMenu { get; set; }

        [JsonProperty("isDeleted")]
        public bool IsDeleted { get; set; }

        [JsonProperty("parentGroup")]
        public string ParentGroupId { get; set; }
    }
}
