﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class IikoOrderRoot
    {
        [JsonProperty("Organization")]
        public string Organization { get; set; }

        [JsonProperty("customer")]
        public IikoCustomer Customer { get; set; }

        [JsonProperty("order")]
        public Order Order { get; set; }
    }

    [JsonObject]
    public class IikoCustomer
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }
    }

    [JsonObject]
    public class Order
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        //"date": "2021-01-21 16:07:00",
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("orderTypeId")]
        public string OrderTypeId { get; set; }

        [JsonProperty("isSelfService")]
        public bool isSelfService { get; set; }

        [JsonProperty("items")]
        public List<Item> Items { get; set; }

        [JsonProperty("paymentItems")]
        public List<PaymentItem> PaymentItems { get; set; }

        [JsonProperty("address")]
        public Address Address { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }
    }

    [JsonObject]
    public class PaymentItem
    {
        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        [JsonProperty("paymentType")]
        public PaymentType PaymentType { get; set; }

        [JsonProperty("isProcessedExternally")]
        public bool IsProcessedExternally { get; set; }

        [JsonProperty("isPreliminary")]
        public bool IsPreliminary { get; set; }

        [JsonProperty("IsExternal")]
        public bool isExternal { get; set; }
    }

    [JsonObject]
    public class Address
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("home")]
        public string Home { get; set; }

        [JsonProperty("apartment")]
        public string Apartment { get; set; }

        //этаж
        [JsonProperty("floor")]
        public string Floor { get; set; }
        
        //Подъезд
        [JsonProperty("entrance")]
        public string Entrance { get; set; }

        [JsonProperty("doorphone")]
        public string DoorPhone { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }
    }

    [JsonObject]
    public class Item
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("modifiers")]
        public List<OrderModifier> Modifiers { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }
    }


    [JsonObject]
    public class OrderModifier
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("groupId")]
        public string GroupId { get; set; }
    }

}
