﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class GroupModifier
    {
        [JsonProperty("maxAmount")]
        public int MaxAmount { get; set; }

        [JsonProperty("minAmount")]
        public int MinAmount { get; set; }

        [JsonProperty("modifierId")]
        public string ModifierId { get; set; }

        [JsonProperty("required")]
        public bool Required { get; set; }

        [JsonProperty("childModifiers")]
        public List<ChildModifier> ChildModifiers { get; set; }

        [JsonProperty("childModifiersHaveMinMaxRestrictions")]
        public bool ChildModifiersHaveMinMaxRestrictions { get; set; }
    }

    [JsonObject]
    public class ChildModifier
    {
        [JsonProperty("maxAmount")]
        public int MaxAmount { get; set; }

        [JsonProperty("minAmount")]
        public int MinAmount { get; set; }

        [JsonProperty("modifierId")]
        public string ModifierId { get; set; }

        [JsonProperty("required")]
        public bool Required { get; set; }

        [JsonProperty("defaultAmount")]
        public int DefaultAmount { get; set; }

        [JsonProperty("hideIfDefaultAmount")]
        public bool HideIfDefaultAmount { get; set; }
    }
}
