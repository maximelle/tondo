﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tondo.WebApp.Core;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    public enum ProductType
    {
        [EnumDisplayName("Блюдо")]
        Dish,
        
        [EnumDisplayName("Модификатор")]
        Modifier

    }
}
