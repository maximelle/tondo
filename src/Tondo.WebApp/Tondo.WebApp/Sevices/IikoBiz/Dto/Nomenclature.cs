﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.IikoBiz.Dto
{
    [JsonObject]
    public class Nomenclature
    {
        [JsonProperty("groups")]
        public List<Group> Groups { get; set; }

        [JsonProperty("productCategories")]
        public List<ProductCategory> ProductCategories { get; set; }

        [JsonProperty("products")]
        public List<Product> Products { get; set; }
    }
}
