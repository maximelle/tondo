﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Newtonsoft.Json;
using Tondo.WebApp.Core;
using Tondo.WebApp.Core.Http;
using Tondo.WebApp.Sevices.IikoBiz.Dto;
using Tondo.WebApp.Sevices.VendorYandex.Dto;

namespace Tondo.WebApp.Sevices.IikoBiz
{
    public class IikoBizService : ClientServiceBase
    {
        private string _token;
        private string _organizationId;

        public IikoBizService(IHttpClientService httpService, string baseUrl, AuthInfo authInfo) : base(httpService, baseUrl, authInfo)
        {

        }

        public override async Task Login()
        {
            try
            {
                var result = await HttpService.GetAsync<string>($"{BaseUrl}/auth/access_token?user_id={AuthInfo.Name}&user_secret={AuthInfo.Password}", null);
                _token =  result;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        public async Task<List<Organization>> GetOrganization()
        {
            try
            {
                var result = await HttpService.GetAsync<List<Organization>>($"{BaseUrl}/organization/list?access_token={_token}", null);
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<PaymentType>> GetPaymentTypes(string organizationId)
        {
            try
            {
                var result = await HttpService.GetAsync<RootPaymentTypes>($"{BaseUrl}/rmsSettings/getPaymentTypes?access_token={_token}&organization={organizationId}", null);
                return result.PaymentTypes;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<OrderType>> GetOrderTypes(string organizationId)
        {
            try
            {
                var result = await HttpService.GetAsync<RootOrderTypes>($"{BaseUrl}/rmsSettings/getOrderTypes?access_token={_token}&organization={organizationId}", null);
                return result.Items;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<Nomenclature> GetNomenclature(string organizationId)
        {
            try
            {
                

                var result = await HttpService.GetAsync<Nomenclature>($"{BaseUrl}/nomenclature/{organizationId}?access_token={_token}&revision=0", null);
                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }


        public async Task<Nomenclature> GetNomenclature()
        {
            try
            {
                if (string.IsNullOrEmpty(_organizationId))
                {
                    var organizations = await GetOrganization();
                    _organizationId = organizations.FirstOrDefault().Id;
                }

                var result = await HttpService.GetAsync<Nomenclature>($"{BaseUrl}/nomenclature/{_organizationId}?access_token={_token}&revision=0", null);
                return result;
            }
            catch (Exception e)
            {
                Logger.Error(e);
                throw;
            }
        }

        public async Task<OrderResponse> CreateOrder(IikoOrderRoot root)
        {
            try
            {
                if (string.IsNullOrEmpty(_organizationId))
                {
                    var organizations = await GetOrganization();
                    _organizationId = organizations.FirstOrDefault().Id;
                }

                //Dictionary<string, string> headers = new Dictionary<string, string>()
                //{
                //    {"cache-control", "no-cache"},
                //    {"Accept", "*/*"},
                //    {"Host", "iiko.biz:9900"},
                //    {"User-Agent", "PostmanRuntime/6.1.6"}
                //};

                root.Organization = _organizationId;

                var stringRequest = JsonConvert.SerializeObject(root, Formatting.None,
                    new JsonSerializerSettings
                    {
                        NullValueHandling = NullValueHandling.Ignore
                    });

                Logger.Info($"Create order request=> {stringRequest}");

                //var result = await HttpService.PostAsync<OrderResponse, IikoOrderRoot>($"{BaseUrl}/orders/add?access_token={_token}", root, headers);
                var res = await  HttpService.PostJson($"{BaseUrl}/orders/add?access_token={_token}", stringRequest);

                Logger.Info($"Create order response=> {res}" );

                var result = JsonConvert.DeserializeObject<OrderResponse>(res);
                return result;
            }
            catch (Exception e)
            {
                Logger.Error("Ошибка создания заказа IIKO", e);
                throw;
            }
        }
    }
}
