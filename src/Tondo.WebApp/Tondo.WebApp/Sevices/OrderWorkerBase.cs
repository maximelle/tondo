﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using log4net;
using Tondo.Data;
using Tondo.WebApp.Sevices.IikoBiz;
using Tondo.WebApp.Sevices.IikoBiz.Dto;

namespace Tondo.WebApp.Sevices
{
    public abstract class OrderWorkerBase
    {
        protected readonly TodoDataContext Context;
        protected readonly IikoBizService IikoBizService;

        protected Nomenclature IikoNomenclature;
        protected List<PaymentType> PaymentItems = new List<PaymentType>();
        protected List<OrderType> OrderTypes = new List<OrderType>();

        private static object SYNC_OBJECT = new object();
        private bool _busy;

        protected ILog Logger;
        
        protected OrderWorkerBase(TodoDataContext dataContext, IikoBizService iikoBizService)
        {
            Context = dataContext;
            IikoBizService = iikoBizService;
            Logger = LogManager.GetLogger(GetType());
        }

        public abstract Task Run();

        protected void SetBusy(bool value)
        {
            lock (SYNC_OBJECT)
            {
                _busy = false;
            }
        }

        protected bool GetBusy()
        {
            lock (SYNC_OBJECT)
            {
                return _busy;
            }
        }
    }
}
