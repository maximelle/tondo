﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.DeliveryClub.Dto
{
    [JsonObject]
    public class RootHeaderMenu
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("payload")]
        public HeaderMenuPayload Payload { get; set; }
    }

    [JsonObject]
    public class HeaderMenuPayload
    {
        [JsonProperty("menus")]
        public List<HeaderMenu> Menus { get; set; }
    }

    [JsonObject]
    public class HeaderMenu
    {
        [JsonProperty("entityTypeId")]
        public string EntityTypeId { get; set; }

        [JsonProperty("menu")]
        public Menu Menu { get; set; }
    }

    [JsonObject]
    public class Menu
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }
}
