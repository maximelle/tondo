﻿
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.DeliveryClub.Dto
{
    [JsonObject]
    public class RootVendors
    {
        [JsonProperty("items")]
        public List<Vendor> Items { get; set; }
    }

    [JsonObject]
    public class Vendor
    {
        [JsonProperty("id")] 
        public int Id { get; set; }

        [JsonProperty("name")] 
        public string Name { get; set; }

        [JsonProperty("chain")] 
        public Chain Chain { get; set; }
    }

    [JsonObject]
    public class Chain
    {
        [JsonProperty("id")] 
        public int Id { get; set; }

        [JsonProperty("chainName")] 
        public string Name { get; set; }
    }
}





