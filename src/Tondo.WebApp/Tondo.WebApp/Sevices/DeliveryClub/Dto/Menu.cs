﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.DeliveryClub.Dto
{
    [JsonObject]
    public class RootMenu
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("payload")]
        public Payload Payload { get; set; }
    }

    [JsonObject]
    public class Payload
    {
        public List<int> rootCategoriesIds { get; set; }
        
        [JsonProperty("categories")]
        public Dictionary<int, Category> Categories { get; set; }

        [JsonProperty("products")]
        public Dictionary<int, DcProduct> Products { get; set; }

        [JsonProperty("ingredientsGroup")]
        public Dictionary<int, IngredientsGroup> IngredientsGroups { get; set; }

        [JsonProperty("ingredients")]
        public Dictionary<int, Ingredient> Ingredients { get; set; }
        //public Products products { get; set; }
        //public Ingredients ingredients { get; set; }
        //public Ingredientsgroup ingredientsGroup { get; set; }
        public int id { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public string statusId { get; set; }
        public DateTime activatedAt { get; set; }
        public string entityTypeId { get; set; }
        public int entityId { get; set; }
    }

    [JsonObject]
    public class DcProduct
    {
        public int id { get; set; }
        public string externalId { get; set; }
        public int relationId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string volume { get; set; }

        [JsonProperty("ingredientGroups")]
        public List<IngGroup> ingredientGroups { get; set; }
    }

    [JsonObject]
    public class IngGroup
    {
        public int id { get; set; }
        [JsonProperty("ingredients")]
        public List<int> Ingridients { get; set; }
    }

    public class Category
    {
        public int id { get; set; }
        public string externalId { get; set; }
        public int relationId { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string imageUrl { get; set; }
        public string deliveryTypeId { get; set; }
        public DateTime createdAt { get; set; }
        //public object[] schedules { get; set; }
        public string entityTypeId { get; set; }
        public int entityId { get; set; }
        //public object[] publicImageUrlList { get; set; }
        //public object[] blocks { get; set; }
        public int parentId { get; set; }
        public List<int> products { get; set; }
        public List<int> childCategories { get; set; }
    }

    public class IngredientsGroup
    {
        public int id { get; set; }
        public string externalId { get; set; }
        public int relationId { get; set; }
        public string name { get; set; }
    }

    public class Ingredient
    {
        public int id { get; set; }
        public string externalId { get; set; }
        public int relationId { get; set; }
        public string name { get; set; }
    }
}
