﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tondo.WebApp.Sevices.DeliveryClub.Dto
{
    
    public class RootOrder
    {
        public Item[] items { get; set; }
    }

    public class Item
    {
        public int totalValue { get; set; }
        public object vat { get; set; }
        public object vatAmount { get; set; }
        public int subtotalValue { get; set; }
        public object minimumDeliveryValue { get; set; }
        public int deliveryFee { get; set; }
        public int deliveryVat { get; set; }
        public object serviceFee { get; set; }
        public object serviceFeeValue { get; set; }
        public object serviceTax { get; set; }
        public object serviceTaxValue { get; set; }
        public object containerCharge { get; set; }
        public string cashOnDeliveryChangeFor { get; set; }
        public Paymenttype paymentType { get; set; }
        public object voucherUsed { get; set; }
        public object[] discountsUsed { get; set; }
        public int payRestaurant { get; set; }
        public string platformForCountry { get; set; }
        public Customeraddress customerAddress { get; set; }
        public int id { get; set; }
        public object oldSystemId { get; set; }
        public object shortCode { get; set; }
        public string code { get; set; }
        public string orderHash { get; set; }
        public int statusCode { get; set; }
        public object deliveryStatus { get; set; }
        public object deliveryRiderStatus { get; set; }
        public bool isUrbanNinja { get; set; }
        public bool trackable { get; set; }
        public object cookingStartTime { get; set; }
        public object cookingStartTimestamp { get; set; }
        public string vendorStatus { get; set; }
        public string status { get; set; }
        public bool isPreOrder { get; set; }
        public bool preorderNotification { get; set; }
        public object declineReason { get; set; }
        public object vendorComment { get; set; }
        public int vendor { get; set; }
        public object vendorBranch { get; set; }
        public string vendorAdditionalPhone { get; set; }
        public string vendorWalkWayToRestaurant { get; set; }
        public bool isDynamicTimeAvailable { get; set; }
        public int isQSR { get; set; }
        public Customer customer { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime expectedDeliveryTime { get; set; }
        public DateTime plannedDeliveryTime { get; set; }
        public int deliveryTime { get; set; }
        public int deliveryTimeAdjustment { get; set; }
        public string customerComment { get; set; }
        public string deliveryComment { get; set; }
        public string deliveryFormattedAddress { get; set; }
        public Deliveryarea deliveryArea { get; set; }
        public DateTime lastStatusUpdate { get; set; }
        public object riderPickupTime { get; set; }
        public object riderName { get; set; }
        public object riderPhone { get; set; }
        public object minimumPickupTime { get; set; }
        public object orderReferenceByVendor { get; set; }
        public object calculationConfiguration { get; set; }
        public object platform { get; set; }
        public object source { get; set; }
        public object averagePreparationTime { get; set; }
        public int averagePackingTime { get; set; }
        public Paymentscheme paymentScheme { get; set; }
        public object products { get; set; }
        public int numberOfPeople { get; set; }
        public object externalId { get; set; }
        public object additionalExternalId { get; set; }
        public Promocode promoCode { get; set; }
        public string customerTotal { get; set; }
        public bool isTakeOut { get; set; }
        public object departmentName { get; set; }
        public object departmentInfo { get; set; }
        public object takeOutTime { get; set; }
        public object specialSubscriptionDiscount { get; set; }
        public object actionsDiscount { get; set; }
        public int isCitymobil { get; set; }
        public object totalWeight { get; set; }
        public bool automatedRiderDispatchingEnabled { get; set; }
        public string expeditionType { get; set; }
        public object deliveryProvider { get; set; }
        public bool vendorSetsRiderPickupTime { get; set; }
        public bool riderPickupTimePreassigned { get; set; }
        public int compensation { get; set; }
    }

    public class Paymenttype
    {
        public int id { get; set; }
        public string name { get; set; }
        public string paymentCode { get; set; }
    }

    public class Customeraddress
    {
        public string addressLine1 { get; set; }
        public object addressLine2 { get; set; }
        public object addressLine3 { get; set; }
        public object addressLine4 { get; set; }
        public object addressLine5 { get; set; }
        public object addressOther { get; set; }
        public string formattedCustomerAddress { get; set; }
        public object room { get; set; }
        public string flatNumber { get; set; }
        public object structure { get; set; }
        public string street { get; set; }
        public string building { get; set; }
        public string intercom { get; set; }
        public string entrance { get; set; }
        public string floor { get; set; }
        public string district { get; set; }
        public string districtType { get; set; }
        public object postcode { get; set; }
        public string cityName { get; set; }
        public string deliveryInstructions { get; set; }
        public object companyName { get; set; }
    }

    public class Customer
    {
        public string id { get; set; }
        public object oldSystemId { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string mobileNumber { get; set; }
        public string maskedMobileNumber { get; set; }
        public string mobileCountryCode { get; set; }
    }

    public class Deliveryarea
    {
        public string id { get; set; }
        public string name { get; set; }
        public object postcode { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

    public class Paymentscheme
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Promocode
    {
        public string discount { get; set; }
        public object[] flags { get; set; }
    }

}
