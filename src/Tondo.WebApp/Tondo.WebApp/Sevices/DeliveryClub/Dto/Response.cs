﻿using Newtonsoft.Json;

namespace Tondo.WebApp.Sevices.DeliveryClub.Dto
{
    [JsonObject]
    public class Response<T>
    {
        [JsonProperty("type")]
        public int Type { get; set; }

        [JsonProperty("payload")]
        public T Payload { get; set; }
    }
}
