﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tondo.WebApp.Sevices.DeliveryClub.Dto
{
    //https://restaurant.delivery-club.ru/api/v2/orders/162075797/products?access_token=YzlkOTE2MjFkZTQ0NDk1OGQyZTcwZDhjZWY3YTU5MzgxNDYxMDdiMmRkMmUyMjAxNGEwMzI0ZDNiOWUyNzQ2NA&locale=ru_RU
    public class RootProductOrder
    {
        public pItem[] items { get; set; }
    }

    public class pItem
    {
        public int id { get; set; }
        public string name { get; set; }
        public int remoteCode { get; set; }
        public object description { get; set; }
        public int totalPrice { get; set; }
        public int originPrice { get; set; }
        public int discount { get; set; }
        public int quantity { get; set; }
        public object vat { get; set; }
        public string measurementTypeId { get; set; }
        public object[] toppings { get; set; }
        public Choice[] choices { get; set; }
        public Variation variation { get; set; }
        public bool isGift { get; set; }
        public string shortName { get; set; }
        public object weight { get; set; }
        public object volume { get; set; }
        public object energyValue { get; set; }
        public bool isBoughtForPoints { get; set; }
    }

    public class Variation
    {
        public int id { get; set; }
        public string name { get; set; }
        public int product { get; set; }
    }

    public class Choice
    {
        public int id { get; set; }
        public string name { get; set; }
        public int price { get; set; }
        public object vat { get; set; }
        public Template template { get; set; }
    }

    public class Template
    {
        public string id { get; set; }
        public string name { get; set; }
    }


}
