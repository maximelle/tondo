﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Authentication;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Tondo.WebApp.Core;
using Tondo.WebApp.Core.Http;
using Tondo.WebApp.Sevices.DeliveryClub.Dto;

namespace Tondo.WebApp.Sevices.DeliveryClub
{
    public class DeliveryClubService : ClientServiceBase
    {
        private string _token;
        private CookieContainer _cookieContainer;

        public DeliveryClubService(IHttpClientService httpService, string baseUrl, AuthInfo authInfo) : base(httpService, baseUrl, authInfo)
        {
        }

        public override async Task Login()
        {
            try
            {
                 _cookieContainer = new CookieContainer();

                //using (var handler = new HttpClientHandler { UseCookies = true })
                using (var handler = new HttpClientHandler { CookieContainer = _cookieContainer})
                using (var client = new HttpClient(handler) { BaseAddress = new Uri(BaseUrl) })
                {
                    var dict = new Dictionary<string, string>();
                    dict.Add("_username", AuthInfo.Name);
                    dict.Add("_password", AuthInfo.Password);
                    dict.Add("_locale", "ru_RU");

                    var req = new HttpRequestMessage(HttpMethod.Post, "/oauth/v2/auth_login_check")
                    {
                        Content = new FormUrlEncodedContent(dict),
                        Headers = {
                            { "cache-control", "no-cache" },
                            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
                            { "Host", "restaurant.delivery-club.ru" },
                            { "accept-encoding", "gzip, deflate" },
                    }
                    };
                    var res0 = await client.SendAsync(req);

                    var req1 = new HttpRequestMessage(HttpMethod.Post, "/oauth/v2/auth_login_check")
                    {
                        Content = new FormUrlEncodedContent(dict),
                        Headers = {
                            { "cache-control", "no-cache" },
                            { "Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9" },
                            { "Host", "restaurant.delivery-club.ru" },
                            { "accept-encoding", "gzip, deflate" },
                        }
                    };

                    var res1 = await client.SendAsync(req1);
                    
                    var message = new HttpRequestMessage(HttpMethod.Get, "/oauth/v2/auth?client_id=vendor_user&response_type=token");


                    //message.Headers.Add("Cookie", "REMEMBERME_SAFE=2021-01-19T17%3A33%3A36%2B03%3A00; REMEMBERME=Rm9vZHBhbmRhXEJ1bmRsZVxWZW5kb3JVc2VyU2VydmljZUJ1bmRsZVxFbnRpdHlcVmVuZG9yVXNlcjpkRzl1Wkc5dGIzTmpiM2RBWjIxaGFXd3VZMjl0OjE2NDI2MDI4MTY6NTdiZTU0ZDI4Y2QxNDFjOWE5NGZlNjhkNWFlYWViMmY5YjZiZTkzYjhkNjMyNTQ3MjA3NmUwNjhjM2M2YWI5OA%3D%3D; " +
                    //                              "VBPHPSESSID=aurd50fo1uqhis0jranivd9geb");
                    var result = await client.SendAsync(message);
                    result.EnsureSuccessStatusCode();
                    string fragment = result.RequestMessage.RequestUri.Fragment;
                    var i0 = fragment.IndexOf("?");
                    if (i0 < 0)
                    {
                        throw new AuthenticationException($"Ошибка аутентификации - {fragment}");
                    }

                    var str0 = fragment.Substring(i0, fragment.Length - i0);
                    var args0 = str0.Split("&");
                    if (args0.Length == 0)
                    {
                        throw new AuthenticationException($"Ошибка аутентификации - {fragment}");
                    }

                    for (int i = 0; i < args0.Length; i++)
                    {
                        var str1 = args0[i];

                        if (string.IsNullOrEmpty(str1))
                        {
                            throw new AuthenticationException($"Ошибка аутентификации - {fragment}");
                        }

                        var args1 = str1.Split("=");
                        if (args1[0] == "access_token")
                        {
                            _token = args1[1];
                            return;
                        }
                    }

                    throw new AuthenticationException($"Ошибка аутентификации - {fragment}");

                }
                //https://restaurant.delivery-club.ru/app?6006f9c75dbf6#/oauth?auth=1&access_token=NzEwM2JhODU4Y2QwZDhkNGI3ZWJjNzc4MmU4NjZkMjcxYmZkNTJhZDM0N2Y2NDdhNjkxNTVlNTU2MDQ1NjQwYw&expires_in=31536000&token_type=bearer

                //using (HttpClient client = new HttpClient())
                //{

                //    var response = client.GetAsync("https://restaurant.delivery-club.ru/oauth/v2/auth?client_id=vendor_user&response_type=token").Result;
                //    var content = response.Content.ReadAsStringAsync().Result;

                //}
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<Menu>> GetMenuHeaders(int chaidId)
        {
            try
            {
                var header = await HttpService.GetAsync<RootHeaderMenu>($"{BaseUrl}/dashboard/im/menus/entities/chain/{chaidId}/all",
                    GetRequestHeaders());

                var result = header.Payload.Menus.Select(m => m.Menu).Where(x => x.Id > 0 && x.Status == "active").ToList();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        //  https://restaurant.delivery-club.ru/api/v2/orders?access_token=ODQzYmE4NzM0Y2E2MjM5NGQyZTJhMzg5Y2Q0YTZkMDI0ZWY2ODNmOWUyMDJiOTMxZjE5NjQ4N2EyZGY3M2YxMA&locale=ru_RU&createdAtSince=2021-03-01+00:00:00&createdAtTill=2021-03-01+23:59:59&include=compensation&limit=10&offset=0&orderBy=-date&paymentTypes=&statusCode=631,65,62,68,612,614,615,621,622,63,64&statusCodeNot=69,40,49,51,71,50,56,57,681,711,712,70&vendorId=82989,84012,93330,94336,95405,102558
        //	https://restaurant.delivery-club.ru/api/v2/orders?access_token=ODQzYmE4NzM0Y2E2MjM5NGQyZTJhMzg5Y2Q0YTZkMDI0ZWY2ODNmOWUyMDJiOTMxZjE5NjQ4N2EyZGY3M2YxMA&locale=ru_RU&_lastSyncMoment="2021-03-02T06:26:32.000Z"&limit=500&modifiedSince=2021-03-01 14:00:00&offset=0&orderBy=-date&statusCode=62&statusCodeNot=69,40,49,51,71,50,56,57,681,711,712,70&vendorId=82989,84012,93330,94336,95405,102558

        public async Task<List<Vendor>> GetVendorList()
        {
            try
            {

                using (var handler = new HttpClientHandler { CookieContainer = _cookieContainer })
                using (var client = new HttpClient(handler) { BaseAddress = new Uri(BaseUrl) })
                {
                    //var str0 = await client.GetStringAsync($"/api/v1/me?access_token={_token}");
                    //var @object = JObject.Parse(str0);
                    //var children = @object.Children().ToList();

                    //foreach ( JToken jToken in children)
                    //{
                    //    if (jToken["name"].ToString() == "acl")
                    //    {

                    //    }
                    //}

                    //var str = //await client.GetStringAsync($"/api/v1/vendors?access_token={_token}");
                    //    //acl":{"vendors":{"82989":["VIEW"],"84012":["VIEW"],"93330":["VIEW"],"94336":["VIEW"],"95405":["VIEW"]}

                    var vendorsString = await client.GetStringAsync($"https://restaurant.delivery-club.ru/api/v1/vendors?access_token={_token}&locale=ru_RU&id=82989,84012,93330,94336,95405");

                    Dictionary<string ,string> dictionary = new Dictionary<string, string>();
                    
                    //HttpService.Get<string>()
                    
                    //var ss = await client.GetStringAsync("dashboard/im/menus/entities/chain/51195/all");

                        //https://restaurant.delivery-club.ru/api/v2/orders?
                          //access_token=YzlkOTE2MjFkZTQ0NDk1OGQyZTcwZDhjZWY3YTU5MzgxNDYxMDdiMmRkMmUyMjAxNGEwMzI0ZDNiOWUyNzQ2NA&locale=ru_RU&_lastSyncMoment=%222021-02-11T10:10:13.000Z%22&limit=500&modifiedSince=2021-02-11+12:00:00&offset=0&orderBy=-date&statusCode=622,621,54,59,591,592,631,64,65,66,67,679,68,42,491,501,611,612,52,63,55,58,613,614,615&statusCodeNot=69,40,49,51,71,50,56,57,681,711,712,70&vendorId=82989,84012,93330,94336,95405

                    //"https://restaurant.delivery-club.ru/dashboard/im/menus/entities/chain/51195/all"

                    var result  = JsonConvert.DeserializeObject<RootVendors>(vendorsString).Items;
                    return result;
                }
                //var dd =  HttpService.Get<string>($"{BaseUrl}", new Dictionary<string, string>());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<RootOrder> GetOrders(DateTime dateTime)
        {
            try
            {
                //&createdAtSince=2021-03-01+00:00:00&createdAtTill=2021-03-01+23:59:59&include=compensation&limit=10&offset=0&orderBy=-date&paymentTypes=&statusCode=631,65,62,68,612,614,615,621,622,63,64&statusCodeNot=69,40,49,51,71,50,56,57,681,711,712,70&vendorId=82989,84012,93330,94336,95405,102558

                 string vendorIds = string.Join(", ", GetVendorList().Result.Select(x => x.Id).ToArray()); 

                string query = $"locale=ru_RU&createdAtSince={dateTime.ToString("yyyy-MM-dd")}+00:00:00&createdAtTill={dateTime.ToString("yyyy-MM-dd")}+23:59:59&include=compensation&limit=100&offset=0&orderBy=-date&paymentTypes=" +
                               $"&statusCode=631,65,62,68,612,614,615,621,622,63,64&statusCodeNot=69,40,49,51,71,50,56,57,681,711,712,70" +
                               $"&vendorId={vendorIds}";

                
                var result = await HttpService.GetAsync<RootOrder>($"{BaseUrl}/api/v2/orders?{query}", GetRequestHeaders());
                

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<RootProductOrder> GetOrderProducts(int orderId)
        {
            try
            {
                var result = await HttpService.GetAsync<RootProductOrder>($"{BaseUrl}/api/v2/orders/{orderId}/products?locale=ru_RU", GetRequestHeaders());

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        //https://restaurant.delivery-club.ru/api/v2/orders/166885307/products?access_token=ODQzYmE4NzM0Y2E2MjM5NGQyZTJhMzg5Y2Q0YTZkMDI0ZWY2ODNmOWUyMDJiOTMxZjE5NjQ4N2EyZGY3M2YxMA&locale=ru_RU

        public async Task<RootMenu> GetMenu(int menuId, int chainId)
        {
            try
            {
                //restaurant.delivery-club.ru/dashboard/im/menus/3011808?entityId=51195&entityTypeId=chain
                var result = await HttpService.GetAsync<RootMenu>($"{BaseUrl}/dashboard/im/menus/{menuId}?entityId={chainId}&entityTypeId=chain", GetRequestHeaders());

                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        //https://restaurant.delivery-club.ru/api/v2/orders
        //?access_token=YjMxYWM0OTM0NDI1Njk4NTIwZDJiMDFmY2VkZGEwOWQ3MzI5ZDNiMTBlZTFkYzQ3YjQ2MzlmNjk0OTgyMjQxYg&locale=ru_RU&id=162339697&statusCodeNot=69,40,49,51,71,50,56,57,681,711,712,70&vendorId=82989,84012,93330,94336,95405



        private Dictionary<string, string> GetRequestHeaders()
        {
            return new Dictionary<string, string>()
            {
                {"Authorization", $"Bearer {_token}" }
            };
        }
    }
}
