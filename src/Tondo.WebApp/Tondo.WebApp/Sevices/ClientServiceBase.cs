﻿using System.Threading.Tasks;
using log4net;
using Tondo.WebApp.Core;
using Tondo.WebApp.Core.Http;

namespace Tondo.WebApp.Sevices
{
    public abstract class ClientServiceBase
    {
        protected ClientServiceBase(IHttpClientService httpService, string baseUrl, AuthInfo authInfo)
        {
            HttpService = httpService;
            BaseUrl = baseUrl;
            AuthInfo = authInfo;

            Logger = LogManager.GetLogger(GetType());
        }

        protected ILog Logger { get; private set; }
        protected string BaseUrl { get; private set; }
        protected IHttpClientService HttpService { get; private set; }
        protected AuthInfo AuthInfo { get; private set; }

        public abstract Task Login();
    }
}
