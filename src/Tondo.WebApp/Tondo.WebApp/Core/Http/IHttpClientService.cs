﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Tondo.WebApp.Core.Http
{
    public interface IHttpClientService
    {
        TResponse Execute<TResponse, TRequest>(TRequest request, string url, string method) where TRequest : class where TResponse : class;
        void Post<TRequest>(TRequest request) where TRequest : class;
        TResponse Get<TResponse>(string url, Dictionary<string, string> headers) where TResponse : class;

        Task<TResponse> GetAsync<TResponse>(string url, Dictionary<string, string> headers) where TResponse : class;
        TResponse PostFormData<TResponse>(string url, Dictionary<string, List<string>> dictionary, Dictionary<string, string> headers)
            where TResponse : class;

        Task<TResponse> PostAsync<TResponse, TRequest>(string url, TRequest request, Dictionary<string, string> headers) where TRequest : class where TResponse : class;
        Task<TResponse> PostAsync1<TResponse, TRequest>(string url, TRequest request, Dictionary<string, string> headers) where TRequest : class where TResponse : class;
        Task<string> PostJson(string url, string request);
    }
}
