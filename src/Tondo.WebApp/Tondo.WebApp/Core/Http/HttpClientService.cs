﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Tondo.WebApp.Core.Http
{
    [Serializable]
    public class HttpClientService : IHttpClientService
    {
        public HttpClientService()
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
        }

        public TResponse Execute<TResponse, TRequest>(TRequest request, string url, string method) where TRequest : class where TResponse : class
        {
            return null;
        }

        public TResponse PostFormData<TResponse>(string url, Dictionary<string, List<string>> dictionary, Dictionary<string, string> headers)
            where TResponse : class
        {
            try
            {
                var formVariables = new List<KeyValuePair<string, string>>();
                foreach (var item in dictionary)
                {
                    foreach (var v in item.Value)
                    {
                        formVariables.Add(new KeyValuePair<string, string>(item.Key, v));
                    }
                }
                var formContent = new FormUrlEncodedContent(formVariables);

                using (HttpClient client = new HttpClient())
                {
                    if (headers != null)
                    {

                        foreach (var item in headers)
                        {
                            client.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }
                    }

                    var response = client.PostAsync($"{url}", formContent).Result;
                    var content = response.Content.ReadAsStringAsync().Result;
                    var tObject = JsonConvert.DeserializeObject<TResponse>(content);

                    return tObject;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<TResponse> PostAsync<TResponse, TRequest>(string url, TRequest request, Dictionary<string, string> headers) where TResponse : class where TRequest : class
        {
            try
            {
                HttpClientHandler handler = new HttpClientHandler()
                {
                    AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate
                };

                using (HttpClient client = new HttpClient(handler))
                {
                    if (headers != null)
                    {
                        foreach (var item in headers)
                        {
                            client.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }
                    }

                    var response = await client.PostAsJsonAsync($"{url}", request);
                    var content = await response.Content.ReadAsStringAsync();
                    var tObject = JsonConvert.DeserializeObject<TResponse>(content);

                    return tObject;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<string> PostJson(string url, string request)
        {
            using (var client = CreateWebClient())
            {
                var result = await client.UploadStringTaskAsync(new Uri(url), "POST",  request);
                return result;
            }
        }

        private WebClient CreateWebClient()
        {
            var client = new WebClient();
            //client.Encoding = Encoding.UTF8;
            client.Headers[HttpRequestHeader.ContentType] = "application/json";

            //client.Credentials = new NetworkCredential(_login, _password); //doesnt work

            //string credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(_login + ":" + _password));
            //client.Headers[HttpRequestHeader.Authorization] = string.Format("Basic {0}", credentials);

            return client;
        }



        public async Task<TResponse> PostAsync1<TResponse, TRequest>(string url, TRequest request, Dictionary<string, string> headers) where TResponse : class where TRequest : class
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    if (headers != null)
                    {
                        foreach (var item in headers)
                        {
                            client.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }
                    }

                    var text = new StringContent(@"{""from"":""2021-01-19T07:00:00+03:00""}");

                    //var response = await client.PostAsJsonAsync($"{url}", request);
                    //var response = await client.PostAsync($"{url}", new StringContent(JsonConvert.SerializeObject(request)) );
                    var response = await client.PostAsync($"{url}", text);
                    var content = await response.Content.ReadAsStringAsync();
                    var tObject = JsonConvert.DeserializeObject<TResponse>(content);

                    return tObject;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public TResponse Get<TResponse>(string url, Dictionary<string, string> headers) where TResponse : class
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    if (headers != null)
                    {
                        foreach (var item in headers)
                        {
                            client.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }
                    }

                    var response = client.GetAsync(url).Result;
                    var content = response.Content.ReadAsStringAsync().Result;
                    string replaceContent = content.Substring(1, content.Length - 2).Replace(@"\", string.Empty);
                    
                    var tObject = JsonConvert.DeserializeObject<TResponse>(replaceContent);
                    return tObject;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<TResponse> GetAsync<TResponse>(string url, Dictionary<string, string> headers) where TResponse : class
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    if (headers != null)
                    {
                        foreach (var item in headers)
                        {
                            client.DefaultRequestHeaders.Add(item.Key, item.Value);
                        }
                    }

                    var response = await client.GetAsync(url);
                    if (response.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new UnauthorizedAccessException("Ошибка аутентификации");
                    }

                    var content = await response.Content.ReadAsStringAsync();
                    //string replaceContent = content.Substring(1, content.Length - 2).Replace(@"\", string.Empty);

                    if (typeof(TResponse) == typeof(string))
                    {
                        string replaceContent = content.Substring(1, content.Length - 2).Replace(@"\", string.Empty);
                        return replaceContent as TResponse;
                    }

                    var tObject = JsonConvert.DeserializeObject<TResponse>(content);
                    return tObject;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void Post<TRequest>(TRequest request) where TRequest : class
        {

        }
    }
}
