﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Tondo.WebApp.Core
{
    public static class AttributeExtensions
    {
        public static string GetNameString(this Enum @enum)
        {
            Type type = @enum.GetType();

            FieldInfo fieldInfo = type.GetField(@enum.ToString());

            var attribs = fieldInfo.GetCustomAttributes(
                typeof(EnumDisplayNameAttribute), false) as EnumDisplayNameAttribute[];


            return attribs != null && attribs.Length > 0 ? attribs[0].DisplayName : null;
        }

        public static string GetShortNameString(this Enum @enum)
        {
            Type type = @enum.GetType();

            FieldInfo fieldInfo = type.GetField(@enum.ToString());

            var attribs = fieldInfo.GetCustomAttributes(
                typeof(EnumDisplayNameAttribute), false) as EnumDisplayNameAttribute[];


            return attribs != null && attribs.Length > 0 ? attribs[0].ShortName : null;
        }
    }
}
