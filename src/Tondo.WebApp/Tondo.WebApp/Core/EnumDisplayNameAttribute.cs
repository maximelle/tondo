﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tondo.WebApp.Core
{
    [AttributeUsage(AttributeTargets.Field)]
    public class EnumDisplayNameAttribute : System.ComponentModel.DisplayNameAttribute
    {
        public string ShortName { get; private set; }

        public EnumDisplayNameAttribute(string name, string shortName)
            : this(name)
        {
            ShortName = shortName;
        }

        public EnumDisplayNameAttribute(string name)
            : base(name)
        {
        }
    }
}
