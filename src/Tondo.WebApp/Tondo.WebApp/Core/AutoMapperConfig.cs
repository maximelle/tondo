﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Configuration;

namespace Tondo.WebApp.Core
{
	public static class AutoMapperConfig
	{
		private static MapperConfigurationExpression expression = new MapperConfigurationExpression();

		public static void RegisterProfiles()
		{
			RegisterProfiles(
				Assembly.GetAssembly(typeof(AutoMapperConfig))
				//Assembly.GetAssembly(typeof(KwmcModule)),
				//Assembly.GetAssembly(typeof(ResourceModule))
				);
		}
		public static void RegisterProfiles(params Assembly[] assemblies)
		{
			RegisterProfiles(EnsureAssembly(assemblies, Assembly.GetCallingAssembly()));
		}
		public static void RegisterProfiles(IEnumerable<Assembly> assemblies)
		{
			GetConfiguration(expression, EnsureAssembly(assemblies, Assembly.GetCallingAssembly()));
			//MapperInitialize(expression);  //configuration =>)
		}
		private static IEnumerable<Assembly> EnsureAssembly(IEnumerable<Assembly> argAssemblies, Assembly callingAssembly)
		{
			// In order for GetCallingAssembly to work, it must be invoked from a public method.
			// This is why the callingAssembly arg is passed from public methods.

			// create an initial default empty array of assemblies in case argAssemblies is null
			var assembliesArray = new Assembly[] { };

			// when argAssemblies is not null, enumerate it as an array to prevent multiple enumerations
			if (argAssemblies != null)
				assembliesArray = argAssemblies.ToArray();

			// when there are no assemblies explicitly defined, return the calling assembly only
			if (!assembliesArray.Any())
				return new[] { callingAssembly };

			// otherwise, return the explicitly defined assemblies
			return assembliesArray;
		}

		private static void GetConfiguration(MapperConfigurationExpression configuration, IEnumerable<Assembly> assemblies)
		{
			foreach (var assembly in assemblies)
			{
				var classlist = GetProfileClassesFrom(assembly);
				foreach (var profileClass in classlist)
					configuration.AddProfile((Profile)Activator.CreateInstance(profileClass));
			}
		}

		private static IEnumerable<Type> GetProfileClassesFrom(Assembly assembly)
		{
			try
			{
				return assembly.GetTypes()
					.Where(t =>
						t != typeof(Profile)
						&& typeof(Profile).IsAssignableFrom(t)
						&& !t.IsAbstract
					);
			}
			catch
			{
				return new Type[] { };
			}
		}

		public static MapperConfigurationExpression mapperConfigurationExpression
		{ get { return expression; } }
		public static void RegisterConfigurations(this Type[] types)
		{
			var automapperProfiles = types
										.Where(x => x.IsSubclassOf(typeof(Profile)))
										.Select(Activator.CreateInstance)
										.OfType<Profile>()
										.ToList();

			automapperProfiles.ForEach(expression.AddProfile);
		}
	}
}
