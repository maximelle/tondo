﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Tondo.Data;
using Tondo.Data.Models;
using Tondo.WebApp.Core;
using Tondo.WebApp.Sevices.IikoBiz;
using Tondo.WebApp.Sevices.IikoBiz.Dto;
using Product = Tondo.WebApp.Models.Product;

namespace Tondo.WebApp.Controllers
{
    public class IikoMenuController : Controller
    {
        private readonly IikoBizService _iikoBizService;
        private readonly TodoDataContext _context;
        private readonly IMapper _mapper;

        public IikoMenuController(IikoBizService iikoBizService, TodoDataContext context, IMapper mapper)
        {
            _iikoBizService = iikoBizService;
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            try
            {
                List<Product> result = new List<Product>();
                var products = _context.IikoProducts.Include(x => x.Group).ToList();

                foreach (var item in products)
                {
                    result.Add(new Product()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        GroupName = item.Group?.Name,
                        IsActive = item.IsDeleted == false,
                        Type = Enum.Parse<ProductType>(item.Type, true).GetNameString()
                    });
                }

                return View(result.OrderBy(x=>x.Name).AsQueryable());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IActionResult> Sync()
        {
            try
            {
                var company = await _iikoBizService.GetOrganization();
                var nomenclature = await _iikoBizService.GetNomenclature(company.First().Id);

                foreach (var g in nomenclature.Groups)
                {
                    var existGroup = _context.IikoGroups.FirstOrDefault(x => x.Id == g.Id);
                    if (existGroup == null)
                    {
                        IikoGroup group = new IikoGroup();
                        group.Id = g.Id;
                        group.Name = g.Name;
                        group.ParentGroupId = g.ParentGroupId;
                        group.IsDeleted = g.IsDeleted;
                        _context.Add(group);
                        _context.SaveChanges();
                    }
                    else
                    {
                        existGroup.Name = g.Name;
                        existGroup.IsDeleted = g.IsDeleted;
                        existGroup.ParentGroupId = g.ParentGroupId;
                        _context.Update(existGroup);
                        _context.SaveChanges();
                    }

                    var items = nomenclature.Products.Where(p => p.ParentGroupId == g.Id).ToList();

                    foreach (var i in items)
                    {
                        var existProduct = _context.IikoProducts.FirstOrDefault(x => x.Id == i.Id);

                        if (existProduct == null)
                        {
                            //if (i.Type == "dish")
                            //{
                            //    IikoProduct product = new IikoProduct();
                            //    product.Id = i.Id;
                            //    product.Name = i.Name;
                            //    product.Type = i.Type;
                            //    product.GroupId = i.ParentGroupId;
                            //    product.IsDeleted = i.IsDeleted;
                            //    _context.Add(product);
                            //    _context.SaveChanges();
                            //}

                            IikoProduct product = new IikoProduct();
                            product.Id = i.Id;
                            product.Name = i.Name;
                            product.Type = i.Type;
                            product.GroupId = i.ParentGroupId;
                            product.IsDeleted = i.IsDeleted;
                            _context.Add(product);
                            _context.SaveChanges();
                        }
                        else
                        {
                            existProduct.Name = i.Name;
                            existProduct.Type = i.Type;
                            existProduct.IsDeleted = i.IsDeleted;
                            existProduct.GroupId = i.ParentGroupId;
                            _context.Update(existProduct);
                            _context.SaveChanges();
                        }
                    }
                }

                var emptyGroupProducts = nomenclature.Products.Where(x => string.IsNullOrEmpty(x.ParentGroupId));

                foreach (var i in emptyGroupProducts)
                {
                    var existProduct = _context.IikoProducts.FirstOrDefault(x => x.Id == i.Id);

                    if (existProduct == null)
                    {
                        IikoProduct product = new IikoProduct();
                        product.Id = i.Id;
                        product.Name = i.Name;
                        product.Type = i.Type;
                        product.GroupId = _context.IikoGroups.FirstOrDefault(x=>x.Id == i.GroupId)?.Id;
                        product.IsDeleted = i.IsDeleted;
                        _context.Add(product);
                        _context.SaveChanges();
                    }
                    else
                    {
                        existProduct.Name = i.Name;
                        existProduct.Type = i.Type;
                        existProduct.IsDeleted = i.IsDeleted;
                        existProduct.GroupId = _context.IikoGroups.FirstOrDefault(x => x.Id == i.GroupId)?.Id;
                        _context.Update(existProduct);
                        _context.SaveChanges();
                    }
                }

                ViewBag.TheResult = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index");
        }
    }
}
