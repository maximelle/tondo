﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tondo.Data;
using Tondo.Data.Models;
using Tondo.WebApp.Core;
using Tondo.WebApp.Models;
using Tondo.WebApp.Sevices.IikoBiz.Dto;
using Tondo.WebApp.Sevices.VendorYandex;
using Product = Tondo.WebApp.Models.Product;

namespace Tondo.WebApp.Controllers
{
    public class YandexMenuController : Controller
    {
        private readonly VendorYandexService _yandexService;
        private readonly TodoDataContext _context;
        private readonly IMapper _mapper;

        public YandexMenuController(VendorYandexService yandexService, TodoDataContext context, IMapper mapper)
        {
            _yandexService = yandexService;
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            try
            {
                List<Product> result = new List<Product>();
                var products = _context.YandexProducts.Include(x => x.Group).Include(y=>y.Place).Include(d=>d.IikoProduct).ToList();

                foreach (var item in products)
                {
                    result.Add(new Product()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        PlaceName = item.Place?.Name,
                        GroupName = item.Group?.Name,
                        IsActive = item.Available,
                        BindingName = GetBindingSting(item),
                        Type = Enum.Parse<ProductType>(item.Type, true).GetNameString()
                    });
                }

                return View(result.OrderBy(x => x.Name).AsQueryable());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private string GetBindingSting(YandexProduct product)
        {
            if (Enum.Parse<ProductType>(product.Type, true) == ProductType.Dish)
            {
                return product.IikoProduct?.Name;
            }

            var iikoIds = _context.YandexBindings.Where(x => x.YandexId == product.Id).Select(t => t.IikoId).ToList();
            var s = string.Join(",",
                _context.IikoProducts.Include(d=>d.Group).ToList().Where(x => iikoIds.Contains(x.Id)).Select(y => $"{y.Name}({SafeGroupName(y.Group)})").ToList());

            return s;
        }

        private string SafeGroupName(IikoGroup group)
        {
            return group?.Name;
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                List<NameModel> list = new List<NameModel>();
                
                ProductEditModel model = new ProductEditModel();
                var p = _context.YandexProducts.Find(id);
                model.Id = p.Id;
                model.Name = p.Name;
                model.BindingId = p.IikoProductId;

                if (Enum.Parse<ProductType>(p.Type, true) == ProductType.Modifier)
                {
                    return RedirectToAction("EditModifier", new {id = id});
                }

                list = _context.IikoProducts.Include(x=>x.Group).ToList().Select(p=> new NameModel(){Id = p.Id, Name = $"{p.Name}({SafeGroupName(p.Group)})"}).OrderBy(x=>x.Name).ToList();
                list.Insert(0, new NameModel(){Id = "", Name = ""});

                ViewBag.ListOfIiko = list;

                return View("Edit", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpPost]
        public IActionResult Edit(ProductEditModel model)
        {
            try
            {
                var yp = _context.YandexProducts.FirstOrDefault(x => x.Id == model.Id);
                if (yp != null)
                {
                    yp.IikoProductId = model.BindingId;
                }

                _context.SaveChanges();

                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpGet]
        public IActionResult EditModifier(string id)
        {
            try
            {
                ModifierEditModel model = new ModifierEditModel();
                var p = _context.YandexProducts.Find(id);
                model.Id = p.Id;
                model.Name = p.Name;
                //model.BindingId = p.IikoProductId;

                var bindings = _context.YandexBindings.Where(x => x.YandexId == id).ToList().Select(y => y.IikoId);

                model.IikoModifierList = _context.IikoProducts.Include(x => x.Group).ToList().
                    Select(p => new SelectListItem() { Value = p.Id.ToString(), Text = $"{p.Name}({SafeGroupName(p.Group)})" , Selected = bindings.Contains(p.Id.ToString())}).OrderBy(x => x.Text).ToList();
                
                
                //list.Insert(0, new NameModel() { Id = "", Name = "" });

                

                return View("EditModifier", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpPost]
        public IActionResult EditModifier(ModifierEditModel model)
        {
            try
            {
                var yp = _context.YandexProducts.FirstOrDefault(x => x.Id == model.Id);
                if (yp != null)
                {
                    var oldBindings = _context.YandexBindings.Where(x => x.YandexId == yp.Id).ToList();
                    _context.RemoveRange(oldBindings);
                    _context.SaveChanges();

                    foreach (var id in model.SelectedIds)
                    {
                        var p = _context.IikoProducts.FirstOrDefault(x => x.Id == id);
                        if (p != null)
                        {
                            YandexBinding b = new YandexBinding()
                            {
                                IikoId = p.Id,
                                YandexId = yp.Id
                            };
                            _context.Add(b);
                        }
                    }

                    _context.SaveChanges();
                }

                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IActionResult> Sync()
        {
            try
            {
                //var products = _context.YandexProducts.Where(p => p.Type == "modifier" && string.IsNullOrEmpty(p.IikoProductId)).ToList();

                //foreach (var product in products)
                //{
                //    var binding = _context.YandexBindings.FirstOrDefault(b => b.YandexId == product.Id);
                //    if (binding != null)
                //    {
                //        var rem = _context.YandexProducts.Where(p => p.Type == "modifier" && p.MenuItemId == product.MenuItemId && p.PlaceId == product.PlaceId
                //                                                     && p.GroupId == product.GroupId
                //                                                     && p.Id != product.Id).ToList();
                //        if (rem.Any())
                //        {
                //            _context.RemoveRange(rem);
                //            _context.SaveChanges();
                //        }
                //    }
                //    else
                //    {
                //        var rem = _context.YandexProducts.Where(p => p.Type == "modifier" && p.MenuItemId == product.MenuItemId && p.PlaceId == product.PlaceId
                //                                                     && p.GroupId == product.GroupId
                //                                                     && p.Id != product.Id).ToList();
                //        if (rem.Any())
                //        {
                //            var bb = _context.YandexBindings.FirstOrDefault(x =>
                //                rem.Select(f => f.IikoProductId).Contains(x.IikoId));

                //            if (bb == null)
                //            {
                //                _context.RemoveRange(rem);
                //                _context.SaveChanges();
                //            }
                //        }
                //    }
                //}

                //return RedirectToAction("Index");

                var places =  await _yandexService.GetRestaurants();
                foreach (var p in places)
                {
                    var exist = _context.YandexPlaces.FirstOrDefault(x => x.Id == p.Id);
                    if (exist == null)
                    {
                        YandexPlace place = new YandexPlace()
                        {
                            Id = p.Id,
                            Name = p.Name,
                            Available = p.IsAvailable
                        };
                        
                        _context.Add(place);
                        _context.SaveChanges();
                    }
                    else
                    {
                        exist.Name = p.Name;
                        exist.Available = p.IsAvailable;
                        _context.Update(exist);
                        _context.SaveChanges();
                    }

                    var menu = await _yandexService.GetMenu(p.Id);

                    foreach (var g in menu.Groups)
                    {
                        var existGroup = _context.YandexGroups.FirstOrDefault(x => x.PlaceId == p.Id && x.Id == g.Id);
                        if (existGroup == null)
                        {
                            YandexGroup group = new YandexGroup()
                            {
                                Id = g.Id,
                                Name = g.Name,
                                PlaceId = p.Id,
                                Available = g.Available
                            };
                            _context.Add(group);
                            _context.SaveChanges();
                        }
                        else
                        {
                            existGroup.Name = g.Name;
                            existGroup.PlaceId = p.Id;
                            existGroup.Available = g.Available;
                            _context.Update(existGroup);
                            _context.SaveChanges();
                        }

                        var items = menu.Items.Where(x => x.GroupId == g.Id);

                        foreach (var i in items)
                        {
                            //var existProduct = _context.YandexProducts.FirstOrDefault(x => x.Id == i.Id);
                            var existProduct = _context.YandexProducts.FirstOrDefault(x => x.Type == "dish" && x.MenuItemId == i.MenuItemId && x.PlaceId == p.Id && x.GroupId == g.Id);
                            if (existProduct == null)
                            {
                                YandexProduct product = new YandexProduct()
                                {
                                    Id = i.Id,
                                    Name = $"{i.Name} {i.Measure} {i.MeasureUnit}",
                                    PlaceId = p.Id,
                                    Available = i.Available,
                                    GroupId = g.Id,
                                    Type = "dish",
                                    MenuItemId = i.MenuItemId
                                };
                                _context.Add(product);
                                _context.SaveChanges();
                            }
                            else
                            {
                                existProduct.Name = $"{i.Name} {i.Measure} {i.MeasureUnit}";
                                existProduct.Available = i.Available;
                                existProduct.PlaceId = p.Id;
                                existProduct.GroupId = g.Id;
                                existProduct.Type = "dish";
                                existProduct.MenuItemId = i.MenuItemId;

                                _context.Update(existProduct);
                                _context.SaveChanges();
                            }
                        }
                    }

                    var emptyGroupItems = menu.Items.Where(x => string.IsNullOrEmpty(x.GroupId)).ToList();

                    foreach (var i in emptyGroupItems)
                    {
                        //var existProduct = _context.YandexProducts.FirstOrDefault(x => x.PlaceId == p.Id && x.Id == i.Id);
                        var existProduct = _context.YandexProducts.FirstOrDefault(x => x.Type == "dish" && x.MenuItemId == i.MenuItemId && x.PlaceId == p.Id);
                        if (existProduct == null)
                        {
                            YandexProduct product = new YandexProduct()
                            {
                                Id = i.Id,
                                Name = $"{i.Name} {i.Measure} {i.MeasureUnit}",
                                PlaceId = p.Id,
                                Available = i.Available,
                                GroupId = i.GroupId,
                                Type = "dish",
                                MenuItemId = i.MenuItemId
                        };
                            _context.Add(product);
                            _context.SaveChanges();
                        }
                        else
                        {
                            existProduct.Name = $"{i.Name} {i.Measure} {i.MeasureUnit}";
                            existProduct.Available = i.Available;
                            existProduct.PlaceId = p.Id;
                            existProduct.GroupId = i.GroupId;
                            existProduct.Type = "dish";
                            existProduct.MenuItemId = i.MenuItemId;

                            _context.Update(existProduct);
                            _context.SaveChanges();
                        }
                    }

                    var groupModifiers = menu.Items.SelectMany(n => n.GroupModifiers).ToList();

                    foreach (var m in groupModifiers)
                    {
                        var existGroup = _context.YandexGroups.FirstOrDefault(x => x.PlaceId == p.Id && x.Id == m.Id);
                        if (existGroup == null)
                        {
                            YandexGroup group = new YandexGroup()
                            {
                                Id = m.Id,
                                Name = m.Name,
                                PlaceId = p.Id,
                                Available = true
                            };
                            _context.Add(group);
                            _context.SaveChanges();
                        }
                        else
                        {
                            existGroup.Name = m.Name;
                            existGroup.PlaceId = p.Id;
                            existGroup.Available = true;
                            _context.Update(existGroup);
                            _context.SaveChanges();
                        }

                        var modifiers = m.Modifiers;
                        if (m.Id == "kj4oh1bv-v4iq3tcpjne-4fscsiof7q2")
                        {

                        }

                        foreach (var sm in modifiers)
                        {
                            //var existProduct = _context.YandexProducts.FirstOrDefault(x => x.PlaceId == p.Id && x.Id == sm.Id);
                            var existProduct = _context.YandexProducts.FirstOrDefault(x => x.Type == "modifier" && x.MenuItemId == sm.MenuItemOptionId && x.PlaceId == p.Id);
                            if (existProduct == null)
                            {
                                var existProduct1 = _context.YandexProducts.FirstOrDefault(x => x.PlaceId == p.Id && x.Id == sm.Id);
                                if (existProduct1 != null)
                                {
                                    sm.Id = Guid.NewGuid().ToString();
                                }

                                YandexProduct product = new YandexProduct()
                                {
                                    Id = sm.Id,
                                    Name = sm.Name,
                                    PlaceId = p.Id,
                                    Available = sm.Available,
                                    GroupId = m.Id,
                                    Type = "modifier",
                                    MenuItemId = sm.MenuItemOptionId
                                };
                                _context.Add(product);
                                _context.SaveChanges();
                            }
                            else
                            {
                                existProduct.Name = sm.Name;
                                existProduct.Available = sm.Available;
                                existProduct.PlaceId = p.Id;
                                existProduct.GroupId = m.Id;
                                existProduct.Type = "modifier";
                                existProduct.MenuItemId = sm.MenuItemOptionId;
                                _context.Update(existProduct);
                                _context.SaveChanges();


                                //if (sm.MenuItemOptionId != existProduct.MenuItemId)
                                //{
                                //    YandexProduct product = new YandexProduct()
                                //    {
                                //        Id = Guid.NewGuid().ToString(),
                                //        Name = sm.Name,
                                //        PlaceId = p.Id,
                                //        Available = sm.Available,
                                //        GroupId = m.Id,
                                //        Type = "modifier",
                                //        MenuItemId = sm.MenuItemOptionId
                                //    };
                                //    _context.Add(product);
                                //    _context.SaveChanges();
                                //}
                                //else
                                //{
                                //    existProduct.Name = sm.Name;
                                //    existProduct.Available = sm.Available;
                                //    existProduct.PlaceId = p.Id;
                                //    existProduct.GroupId = m.Id;
                                //    existProduct.Type = "modifier";
                                //    existProduct.MenuItemId = sm.MenuItemOptionId;
                                //    _context.Update(existProduct);
                                //    _context.SaveChanges();
                                //}

                            }
                        }
                    }

                    var allModifiers = groupModifiers.SelectMany(x => x.Modifiers).Select(y=>y.MenuItemOptionId).ToList();
                    var removedModifiers =  _context.YandexProducts.Where(t => t.PlaceId == p.Id && t.Type == "modifier" && allModifiers.Contains(t.MenuItemId) == false).ToList();
                    _context.RemoveRange(removedModifiers);

                    var allProducts = menu.Items.Select(x => x.Id).ToList();
                    var removedProducts = _context.YandexProducts.Where(t => t.PlaceId == p.Id && t.Type == "dish" && allProducts.Contains(t.Id) == false).ToList();
                    _context.RemoveRange(removedProducts);

                    _context.SaveChanges();
                }
                ViewBag.TheResult = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index");
        }

    }
}
