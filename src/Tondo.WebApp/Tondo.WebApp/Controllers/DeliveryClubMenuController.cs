﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Tondo.Data;
using Tondo.Data.Models;
using Tondo.WebApp.Core;
using Tondo.WebApp.Models;
using Tondo.WebApp.Sevices.DeliveryClub;
using Tondo.WebApp.Sevices.IikoBiz;
using Tondo.WebApp.Sevices.IikoBiz.Dto;
using Product = Tondo.WebApp.Models.Product;

namespace Tondo.WebApp.Controllers
{
    public class DeliveryClubMenuController : Controller
    {
        private readonly DeliveryClubService _deliveryClubService;
        private readonly TodoDataContext _context;
        private readonly IMapper _mapper;

        public DeliveryClubMenuController(DeliveryClubService deliveryClubService, TodoDataContext context, IMapper mapper)
        {
            _deliveryClubService = deliveryClubService;
            _context = context;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            try
            {
                List<Product> result = new List<Product>();
                var products = _context.DeliveryClubProducts.Include(x => x.Group)
                    .Include(y => y.Vendor).Include(d => d.IikoProduct).ToList();

                foreach (var item in products)
                {
                    result.Add(new Product()
                    {
                        Id = item.Id,
                        Name = item.Name,
                        PlaceName = item.Vendor?.ChainName,
                        GroupName = item.Group?.Name,
                        BindingName = GetBindingSting(item),
                        Type = Enum.Parse<ProductType>(item.Type, true).GetNameString(),
                        IsActive = true
                    });
                }

                return View(result.OrderBy(x => x.Name).AsQueryable());
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private string GetBindingSting(DeliveryClubProduct product)
        {
            if (Enum.Parse<ProductType>(product.Type, true) == ProductType.Dish)
            {
                return product.IikoProduct?.Name;
            }

            var iikoIds = _context.YandexBindings.Where(x => x.YandexId == product.Id).Select(t => t.IikoId).ToList();
            var s = string.Join(",",
                _context.IikoProducts.Include(d => d.Group).ToList().Where(x => iikoIds.Contains(x.Id)).Select(y => $"{y.Name}({SafeGroupName(y.Group)})").ToList());

            return s;
        }

        private string SafeGroupName(IikoGroup group)
        {
            return group?.Name;
        }

        [HttpGet]
        public IActionResult Edit(string id)
        {
            try
            {
                List<NameModel> list = new List<NameModel>();

                ProductEditModel model = new ProductEditModel();
                var p = _context.DeliveryClubProducts.Find(id);
                model.Id = p.Id;
                model.Name = p.Name;
                model.BindingId = p.IikoProductId;

                if (Enum.Parse<ProductType>(p.Type, true) == ProductType.Modifier)
                {
                    return RedirectToAction("EditModifier", new { id = id });
                }

                list = _context.IikoProducts.Include(x => x.Group).ToList().Select(p => new NameModel() { Id = p.Id, Name = $"{p.Name}({SafeGroupName(p.Group)})" }).OrderBy(x => x.Name).ToList();
                list.Insert(0, new NameModel() { Id = "", Name = "" });

                ViewBag.ListOfIiko = list;

                return View("Edit", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [HttpPost]
        public IActionResult Edit(ProductEditModel model)
        {
            try
            {
                var yp = _context.DeliveryClubProducts.FirstOrDefault(x => x.Id == model.Id);
                if (yp != null)
                {
                    yp.IikoProductId = model.BindingId;
                }

                _context.SaveChanges();

                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpGet]
        public IActionResult EditModifier(string id)
        {
            try
            {
                ModifierEditModel model = new ModifierEditModel();
                var p = _context.DeliveryClubProducts.Find(id);
                model.Id = p.Id;
                model.Name = p.Name;
                //model.BindingId = p.IikoProductId;

                var bindings = _context.YandexBindings.Where(x => x.YandexId == id).ToList().Select(y => y.IikoId);

                model.IikoModifierList = _context.IikoProducts.Include(x => x.Group).ToList().
                    Select(p => new SelectListItem() { Value = p.Id.ToString(), Text = $"{p.Name}({SafeGroupName(p.Group)})", Selected = bindings.Contains(p.Id.ToString()) }).OrderBy(x => x.Text).ToList();


                //list.Insert(0, new NameModel() { Id = "", Name = "" });



                return View("EditModifier", model);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpPost]
        public IActionResult EditModifier(ModifierEditModel model)
        {
            try
            {
                var yp = _context.DeliveryClubProducts.FirstOrDefault(x => x.Id == model.Id);
                if (yp != null)
                {
                    var oldBindings = _context.YandexBindings.Where(x => x.YandexId == yp.Id).ToList();
                    _context.RemoveRange(oldBindings);
                    _context.SaveChanges();

                    foreach (var id in model.SelectedIds)
                    {
                        var p = _context.IikoProducts.FirstOrDefault(x => x.Id == id);
                        if (p != null)
                        {
                            YandexBinding b = new YandexBinding()
                            {
                                IikoId = p.Id,
                                YandexId = yp.Id
                            };
                            _context.Add(b);
                        }
                    }

                    _context.SaveChanges();
                }

                return RedirectToAction("Index");

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<IActionResult> Sync()
        {
            try
            {
                var vendors = await _deliveryClubService.GetVendorList();

                string vendorDbId;

                foreach (var p in vendors)
                {
                    var exist = _context.DeliveryClubVendors.FirstOrDefault(x => x.VendorId == p.Id);
                    if (exist == null)
                    {

                        vendorDbId = Guid.NewGuid().ToString();
                        DeliveryClubVendor place = new DeliveryClubVendor()
                        {
                            Id = vendorDbId,
                            VendorId = p.Id,
                            Name = p.Name,
                            ChainId = p.Chain.Id,
                            ChainName = p.Chain.Name
                        };

                        _context.Add(place);
                        _context.SaveChanges();
                    }
                    else
                    {
                        exist.Name = p.Name;
                        exist.ChainName = p.Chain.Name;
                        exist.ChainId = p.Chain.Id;
                        vendorDbId = exist.Id;
                        _context.Update(exist);
                        _context.SaveChanges();
                    }

                    var menuHeaders = await _deliveryClubService.GetMenuHeaders(p.Chain.Id);

                    foreach (var mh in menuHeaders)
                    {
                        var menu = await _deliveryClubService.GetMenu(mh.Id, p.Chain.Id);

                        foreach (var kvpCategory in menu.Payload.Categories)
                        {
                            var existGroup = _context.DeliveryClubGroups.FirstOrDefault(x => x.VendorId == vendorDbId && x.DeliveryClubId == kvpCategory.Key);
                            string dbGroupId;
                            if (existGroup == null)
                            {
                                dbGroupId = Guid.NewGuid().ToString();

                                DeliveryClubGroup group = new DeliveryClubGroup()
                                {
                                    Id = dbGroupId,
                                    DeliveryClubId = kvpCategory.Key,
                                    Name = kvpCategory.Value.name,
                                    ExternalId = kvpCategory.Value.externalId,
                                    VendorId = vendorDbId,
                                };
                                _context.Add(group);
                                _context.SaveChanges();
                            }
                            else
                            {
                                existGroup.Name = kvpCategory.Value.name;
                                existGroup.ExternalId = kvpCategory.Value.externalId;
                                dbGroupId = existGroup.Id;
                                _context.Update(existGroup);
                                _context.SaveChanges();
                            }

                            var items = menu.Payload.Products.Where(x => kvpCategory.Value.products.Contains(x.Key));

                            foreach (var i in items)
                            {
                                var existProduct = _context.DeliveryClubProducts.FirstOrDefault(x => x.DeliveryClubId == i.Key);
                                if (existProduct == null)
                                {
                                    DeliveryClubProduct product = new DeliveryClubProduct()
                                    {
                                        Id = Guid.NewGuid().ToString(),
                                        Name = $"{i.Value.name} {i.Value.volume}",
                                        VendorId = vendorDbId,
                                        GroupId = dbGroupId,
                                        Type = "dish",
                                        DeliveryClubId = i.Key,
                                        ExternalId = i.Value.externalId
                                    };
                                    _context.Add(product);
                                    _context.SaveChanges();
                                }
                                else
                                {
                                    existProduct.Name = $"{i.Value.name} {i.Value.volume}";
                                    existProduct.VendorId = vendorDbId;
                                    existProduct.GroupId = dbGroupId;
                                    existProduct.Type = "dish";
                                    existProduct.ExternalId = i.Value.externalId;

                                    _context.Update(existProduct);
                                    _context.SaveChanges();
                                }
                            }

                        }

                        foreach (var kvpCategory in menu.Payload.IngredientsGroups)
                        {
                            var existGroup = _context.DeliveryClubGroups.FirstOrDefault(x => x.VendorId == vendorDbId && x.DeliveryClubId == kvpCategory.Key);
                            string dbGroupId;
                            if (existGroup == null)
                            {
                                dbGroupId = Guid.NewGuid().ToString();

                                DeliveryClubGroup group = new DeliveryClubGroup()
                                {
                                    Id = dbGroupId,
                                    DeliveryClubId = kvpCategory.Key,
                                    Name = kvpCategory.Value.name,
                                    ExternalId = kvpCategory.Value.externalId,
                                    VendorId = vendorDbId,
                                };
                                _context.Add(group);
                                _context.SaveChanges();
                            }
                            else
                            {
                                existGroup.Name = kvpCategory.Value.name;
                                existGroup.ExternalId = kvpCategory.Value.externalId;
                                dbGroupId = existGroup.Id;
                                _context.Update(existGroup);
                                _context.SaveChanges();
                            }
                            //ТОВАРЫ
                        }


                        var InItems = menu.Payload.Ingredients;

                            //id: 304468712, relationId: "304283477", name: "Чесночный

                        foreach (var kvp in InItems)
                        {

                            string dbGroupId = string.Empty;

                            var gg0 = menu.Payload.Products.Where(p => p.Value.ingredientGroups != null ).ToList();

                            var gg = menu.Payload.Products.FirstOrDefault(p =>
                                p.Value.ingredientGroups != null && p.Value.ingredientGroups.Any(ii => ii.Ingridients.Contains(kvp.Key)));

                            if (gg.Value != null)
                            {
                                var first = gg.Value.ingredientGroups.FirstOrDefault(ggg => ggg.Ingridients.Contains(kvp.Key));
                                if (first != null)
                                {
                                    var gr = _context.DeliveryClubGroups.FirstOrDefault(t =>
                                        t.DeliveryClubId == first.id && t.VendorId == vendorDbId);

                                    if (gr != null)
                                    {
                                        dbGroupId = gr.Id;
                                    }
                                }
                            }

                            var existProduct = _context.DeliveryClubProducts.FirstOrDefault(x => x.DeliveryClubId == kvp.Key);
                            if (existProduct == null)
                            {
                                DeliveryClubProduct product = new DeliveryClubProduct()
                                {
                                    Id = Guid.NewGuid().ToString(),
                                    Name = $"{kvp.Value.name}",
                                    VendorId = vendorDbId,
                                    GroupId = dbGroupId,
                                    Type = "modifier",
                                    DeliveryClubId = kvp.Key,
                                    ExternalId = kvp.Value.externalId
                                };
                                _context.Add(product);
                                _context.SaveChanges();
                            }
                            else
                            {
                                existProduct.Name = kvp.Value.name;
                                existProduct.VendorId = vendorDbId;
                                existProduct.GroupId = dbGroupId;
                                existProduct.Type = "modifier";
                                existProduct.ExternalId = kvp.Value.externalId;

                                _context.Update(existProduct);
                                _context.SaveChanges();
                            }
                        }


                    }
                }

                ViewBag.TheResult = true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return RedirectToAction("Index");
        }
    }
}
