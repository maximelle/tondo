﻿using AutoMapper;
using Tondo.Data.Models;
using Tondo.WebApp.Sevices.VendorYandex.Dto;

namespace Tondo.WebApp.Mapping
{
    public class YandexModelProfile : Profile
    {
        public YandexModelProfile()
        {
            CreateMap<YandexGroup, Group>().ReverseMap();
            CreateMap<YandexProduct, Product>().ReverseMap();
        }
    }
}
